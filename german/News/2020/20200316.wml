<define-tag pagetitle>Die offiziellen Kommunikationskanäle von Debian</define-tag>
<define-tag release_date>2020-03-16</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="c1e997409ebbfee3f612efc60c0f1e3fe81c7e9c" maintainer="Erik Pfannenstein"

<p>
Von Zeit zu Zeit werden wir in Debian nach unseren offiziellen 
Kommunikationskanälen und unserer Beziehung zu verschiedenen Personen, die 
ähnlich benannte Websites betreiben, gefragt.
</p>

<p>
Unser zentrales Kommunikationsmedium ist die Debian-Hauptwebsite unter 
<a href="https://www.debian.org">www.debian.org</a>. Wer Informationen über 
aktuelle Veranstaltungen und den Entwicklungsfortschritt in der Gemeinschaft 
sucht, wird im <a href="https://www.debian.org/News/">Nachrichtenbereich</a> 
der Website fündig.

Für weniger formale Ankündigungen haben wir ein offizielles Blog namens 
<a href="https://bits.debian.org">Bits from Debian</a> eingerichtet. Auf 
<a href="https://micronews.debian.org">Debian micronews</a> gibt es 
Kurznachrichten.
</p>

<p>
Unser offizieller Newsletter
<a href="https://www.debian.org/News/weekly/">Debian Project News</a> 
und alle offiziellen Ankündigungen werden doppelt auf der Website und den 
Mailinglisten 
<a href="https://lists.debian.org/debian-announce/">debian-announce</a> bzw.
<a href="https://lists.debian.org/debian-news/">debian-news</a> verbreitet.
Das Einsenden von Beiträgen auf diesen Listen ist eingeschränkt.
</p>

<p>
Bei dieser Gelegenheit wollen wir auch gleich aufzeigen, wie das 
Debian-Projekt, kurz Debian, strukturiert ist.</p>

<p>
Die Struktur von Debian wird von unserer 
<a href="https://www.debian.org/devel/constitution">Satzung</a> vorgegeben.
Die Direktoren und delegierten Mitglieder werden auf der Seite 
<a href="https://www.debian.org/intro/organization">Organisationsstruktur</a> 
aufgezählt. Die weiteren Teams sind auf der 
<a href="https://wiki.debian.org/Teams">Teams</a>-Seite aufgeführt.
</p>

<p>
Die vollständige Liste der offiziellen Debian-Mitglieder ist in unserer 
<a href="https://nm.debian.org/members">Mitgliedsübersicht</a> zu finden, 
eine weitere Liste aller, die zu Debian beigetragen haben, unter
<a href="https://contributors.debian.org">Contributors list</a>.
</p>

<p>
Sollten Sie Fragen haben, sind Sie jederzeit eingeladen, sich (auf Englisch) 
an unser Presseteam unter 
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt; zu wenden.
</p>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern und 
Entwicklerinnen Freier Software, die ihre Zeit und Bemühungen einbringen, um 
das vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a> oder schicken Sie eine E-Mail 
(auf Englisch) an &lt;press@debian.org&gt;</p>
