#use wml::debian::template title="Menschen: Wer wir sind und was wir machen" MAINPAGE="true"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Erik Pfannenstein"

#include "$(ENGLISHDIR)/releases/info"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">Wie das alles begann</a>
    <li><a href="#devcont">Entwickler und Beitragende</a>
    <li><a href="#supporters">Individuen und Organisationen, die Debian unterstützen</a>
    <li><a href="#users">Debian-Anwender</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Weil so viele Leute gefragt haben: Debian wird <span style="white-space: nowrap;">/&#712;de.bi.&#601;n/</span> ausgesprochen. Es ist nach seinem Gründer Ian Murdock und seiner Frau Debra benannt.</p>
</aside>

<h2><a id="history">Wie das alles begann</a></h2>

<p>
Es war August 1993, als Ian Murdock mit der Arbeit an einem neuen Betriebssystem
anfing. Es sollte offen sein, ganz im Geiste von Linux und GNU. Er verschickte
eine offene Einladung an andere Software-Entwickler und lud sie ein, bei einer
Software-Distribution mitzumachen, die auf dem damals noch recht jungen Linux-Kernel
basieren sollte. Debian sollte sorgfältig zusamemngestellt und genauso
gewissenhaft betreut und unterstützt werden. Dabei sollte es ein offenes
Design aufweisen und Beiträge und Unterstützung aus der
Freie-Software-Gemeinschaft einfließen lassen.
</p>

<p>
Es begann als kleine, eingeschworene Gruppe von Hackern Freier Software
und wuchs zu einer riesigen, gut organisierten Gruppe aus Entwicklern,
Beitragenden und Anwendern an.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="$(DOC)/manuals/project-history/">Lesen Sie die komplette Geschichte</a></button></p>

<h2><a id="devcont">Entwickler und Beitragende</a></h2>

<p>
Debian ist eine Freiwilligen-Organisation durch und durch. Mehr als eintausend
aktive Entwickler verteilen sich
<a href="$(DEVEL)/developers.loc">auf der ganzen Welt</a> und arbeiten in ihrer
Freizeit an Debian. Wenige von uns haben sich schon mal persönlich
getroffen. Stattdessen kommunizieren wir hauptsächlich über E-Mail (die
E-Mail-Verteiler auf <a href="https://lists.debian.org/">lists.debian.org</a>)
und IRC (Kanal #debian auf irc.debian.org).
</p>


<p>
Die vollständige Liste der offiziellen Debian-Mitglieder ist auf
<a href="https://nm.debian.org/members">nm.debian.org</a> zu finden; 
<a href="https://contributors.debian.org">contributors.debian.org</a>
listet sämtliche Beitragenden und Teams auf, die an der Debian-Distribution
mitarbeiten.</p>

<p>Das Debian-Projekt hat eine umsichtig <a href="organization">aufgebaute
Struktur</a>. Wenn Sie Debian mal genauer von innen betrachten möchten,
besuchen Sie doch die <a href="$(DEVEL)/">Entwickler-Ecke</a>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">Lesen Sie über unsere Philosophie</a></button></p>

<h2><a id="supporters">Individuen und Organisationen, die Debian unterstützen</a></h2>

<p>Zur Debian-Gemeinschft gehören neben den Entwicklern, Unterstützern sowie
viele andere Einzelpersonen und Organisationen auch:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Hosting- und Hardware-Sponsoren</a></li>
  <li><a href="../mirror/sponsors">Spiegel-Sponsoren</a></li>
  <li><a href="../partners/">Entwicklungs- und Servicepartner</a></li>
  <li><a href="../consultants">Berater</a></li>
  <li><a href="../CD/vendors">Händler von Debian-Installationsmedien</a></li>
  <li><a href="../distrib/pre-installed">Computer-Händler, die vorinstallierte Debian-Maschinen verkaufen</a></li>
  <li><a href="../events/merchandise">Händler von Fanartikeln</a></li>
</ul>

<h2><a id="users">Debian-Anwender</a></h2>

<p>
Debian wird von vielfältigen Organisationen klein und groß sowie von tausenden
Einzelpersonen eingesetzt. Auf unserer
<a href="../users/">Wer verwendet Debian?</a>-Seite finden Sie eine Liste von
Bildungseinrichtungen, kommerziellen Unternehmen und gemeinnützigen
Organisationen sowie Regierungsbehörden, die beschrieben haben, wo und warum
sie Debian verwenden.</p>
