#use wml::debian::cdimage title="Baixando as imagens do CD/DVD Debian via HTTP/FTP" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="f4fe84f1063f34ac4bfb75eb963e6fa71ba0e642"

<div class="tip">
<p><strong>Por favor não baixe as imagens de CD ou DVD com seu navegador web
da mesma maneira que você baixa outros arquivos!</strong> O motivo é que,
se o download for interrompido, a maioria dos navegadores não lhe permitirá
reiniciar do ponto onde falhou.</p>
</div>

<p>Em vez disso, use por favor um aplicativo que suporte a retomada -
normalmente descrito como um <q>gerenciador de download</q>. Existem muitos
plug-ins para navegador que fazem esse trabalho, ou você pode querer instalar
um programa separado. No Linux/Unix, você pode usar
<a href="http://aria2.sourceforge.net/">aria2</a>,
<a href="http://dfast.sourceforge.net/">wxDownload Fast</a> ou
(na linha de comando) <q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q> ou
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>. Existem
muito mais opções listadas em uma
<a href="https://en.wikipedia.org/wiki/Comparison_of_download_managers">comparação
de gerenciadores de download</a>.</p>

<p>As seguintes imagens do Debian estão disponíveis para serem baixadas:</p>

<ul>

  <li><a href="#stable">Imagens oficiais de CD/DVD da versão
  <q>estável (stable)</q></a></li>
  
  <li><a href="#firmware">Imgens <b>não oficiais</b> de CD/DVD para a
  <q>estável (stable)</q> com firmwares <b>não livres</b> incluídos</a></li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Imagens
  oficiais de CD/DVD da versão <q>teste (testing)</q> (<em>regeradas
  semanalmente</em>)</a></li>

<comment>
  <li>Imagens não oficiais do CD/DVD da versão <q>teste (testing)</q> e
  <q>instável (unstable)</q> por fsn://HU &mdash; <a href="#unofficial">veja abaixo</a>
  </li>
</comment>

</ul>

<p>Veja também:</p>
<ul>

  <li>Uma <a href="#mirrors">lista completa de espelhos <tt>debian-cd/</tt></a></li>

  <li>Para imagens de <q>instalação via rede</q> (150-300&nbsp;MB)
  veja a página <a href="../netinst/">instalação via rede</a>.</li>

  <li>Para imagens <q>netinst</q> da versão teste (<q>testing</q>),
  tanto das construções diárias quanto dos snapshots que sabemos
  estar funcionando, veja na <a href="$(DEVEL)/debian-installer/">página do
  instalador do Debian (<q>Debian-Installer</q>)</a>.</li>

</ul>

<hr />

<h2><a name="stable">Imagens oficiais de CD/DVD da distribuição estável (<q>stable</q>)</a></h2>

<p>Para instalar o Debian em uma máquina sem conexão com a Internet,
é possível usar as imagens de CD (700&nbsp;MB cada) ou as imagens de DVD
(4,4&nbsp;GB cada). Baixe o primeiro arquivo de imagem de CD ou DVD, grave-o
usando um gravador de CD/DVD (ou um pendrive USB, nos portes i386 e amd64), e
então reinicialize a partir dessa mídia.</p>

<p>O <strong>primeiro</strong> disco de CD/DVD contém todos os arquivos
necessários para instalar um sistema Debian padrão.<br />
Para evitar baixar dados desnecessariamente, por favor, <strong>não</strong>
baixe outros arquivos de imagem de CD ou DVD a menos que você saiba que
precisa de pacotes deles.</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>Os seguintes links apontam para arquivos de imagem que têm até 650&nbsp;MB
de tamanho, tornando-os adequados para gravação em mídia CD-R(W) normal:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>Os seguintes links apontam para arquivos de imagem que têm até 4,4&nbsp;GB
de tamanho, tornando-os adequados para gravação em mídias DVD-R/DVD+R normais
e similares:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Tenha certeza de ter olhado a documentação antes de instalar.
<strong>Se você quer ler somente um único documento</strong> antes da
instalação, leia nosso <a href="$(HOME)/releases/stable/i386/apa">tutorial de instalação</a>,
um rápido passo a passo do processo de instalação. Outras documentações úteis
incluem:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Guia de Instalação</a>,
    as instruções de instalação detalhadas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Documentação do
    instalador do Debian (<q>Debian-Installer</q>)</a>, incluindo o FAQ com questões e respostas comuns</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Errata do
    instalador do Debian (<q>Debian-Installer</q>)</a>, a lista de problemas conhecidos no instalador</li>
</ul>

<hr />

# Tradutores: o seguinte parágrafo existe (nesta ou em uma forma semelhante) várias vezes nos webwml,
# portanto, tente manter as traduções consistentes. Veja:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">Imagens não oficiais de CD/DVD com firmwares não livres incluídos</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Se algum hardware em seu sistema <strong>requer que firmware não livre seja
carregado</strong> com o controlador do dispositivo, você pode usar um dos
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
arquivos tarball de pacotes de firmware comuns</a> ou baixar uma imagem
<strong>não oficial</strong> que inclui esses firmwares <strong>não livres</strong>.
Instruções de como usar os arquivos tarball e informações gerais sobre como
carregar um firmware durante uma instalação podem ser encontradas no
<a href="../releases/stable/amd64/ch06s04">guia de instalação</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">imagens
de instalação não oficiais para <q>estável (stable)</q> com firmwares
incluídos</a>
</p>
</div>

<hr />

<h2><a name="mirrors">Espelhos registrados do repositório <q>debian-cd</q></a></h2>

<p>Note que <strong>alguns espelhos não estão atualizados</strong> &mdash;
antes de baixar, verifique se o número da versão das imagens é o mesmo
daquele listado <a href="../#latest">neste site</a>!
Além disto, note que muitos sites não refletem o conjunto completo de imagens
(especialmente as imagens em DVD) devido a seu tamanho.</p>

<p><strong>Em caso de dúvida, use o <a
href="https://cdimage.debian.org/debian-cd/">servidor primário de imagens
de CD</a> na Suécia</strong> ou tente o <a href="http://debian-cd.debian.net/">
seletor automático experimental de espelhos</a> que irá redirecioná-lo para um
espelho que sabemos que possui a versão atual.</p>

<p>Você está interessado(a) em oferecer imagens do CD Debian no seu servidor
de arquivos (<q>mirror</q>)? Se sua resposta for sim, veja as
<a href="../mirroring/">instruções sobre como montar um espelho
de imagens de CD (<q>mirror</q>)</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
