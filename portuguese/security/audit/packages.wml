#use wml::debian::template title="Diretrizes para priorização da auditoria de pacotes"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="8536cf419447d00f034a8e3ad9efa6a243462fe7"

<p>Ao executar uma auditoria na distribuição Debian, uma das
primeiras questões é decidir quais pacotes examinar.</p>

<p>Idealmente todos os pacotes deveriam ser examinados, mas devido ao tamanho
do repositório deve haver uma maneira simples de priorizar o
trabalho.</p>

<p>De acordo com um simples conjunto de diretrizes, os pacotes que devem ser
examinados prioritariamente são:

<ol>
<li>Qualquer binário que é instalado com setuid ou setgid</li>

<li>Qualquer coisa que provê um serviço pela rede.</li>

<li>Qualquer script CGI/PHP acessível remotamente.</li>

<li>Qualquer coisa que contenha um cronjob ou outro script automatizado que
seja executado com privilégio de root.</li>

</ol>

<p>Pacotes populares devem geralmente receber maior prioridade já que
qualquer problema neles afetará um número maior de usuários(as).</p>

<p>O <a href="https://popcon.debian.org/">concurso de popularidade do Debian</a>
mantém uma pesquisa contínua para mostrar quais são os pacotes mais
populares entre os voluntários(as) do concurso.</p>

<p>Veja particularmente <a
href="https://popcon.debian.org/by_vote">os pacotes ordenados por voto</a>.
Essa lista <q>por voto</q> classifica os pacotes por quão frequentemente eles
são usados pelas pessoas que participam da pesquisa.</p>

<p>Se um pacote é importante para segurança, especialmente se é enquadrado em
um dos critérios acima, e é popular (de acordo com a pesquisa de popularidade),
então ele é <em>definitivamente</em> um candidato para auditoria.</p>


<h2>Binários com <tt>setuid</tt> e <tt>setgid</tt></h2>

<p>Binários com <tt>setuid</tt> e <tt>setgid</tt> são tradicionalmente
alvos de auditoria de segurança, pois o comprometimento de binários vulneráveis
com quaisquer dessas permissões pode permitir que um(a) usuário(a) local obtenha
privilégios que ele(a) não deveria ter.</p>

<p>Para ajudar nessa busca, há uma lista com todos os binários com setuid e
setgid os quais estão presentes na versão estável atual.</p>

<ul>

<li><a href="https://lintian.debian.org/tags/setuid-binary.html">Relatório
gerado pelo lintian de todos binários com setuid no Debian</a></li>

<li><a href="https://lintian.debian.org/tags/setgid-binary.html">Relatório
gerado pelo lintian de todos binários com setgid no Debian</a></li>

</ul>

# TODO (jfs): the above does not provide the same information as was available at
# http://shellcode.org/Setuid/
# ask Steve Kemp to move this feature to a Debian-administered machine?
# (or to the Alioth project)
#

<p>Quando se trata de escolher os binários, é importante atentar-se
ao fato de que alguns deles são mais sensíveis na segurança do que outros.
Os binários com setuid(root) devem ser examinados antes de setgid(games) e
setuid(bugs) por exemplo.</p>


<h2>Servidores de rede</h2>

<p>Servidores de rede são outra fonte óbvia de inspiração quando
se trata de realizar uma auditoria de segurança já que uma vulnerabilidade
explorável neles pode permitir que atacantes comprometam máquinas remotamente.
</p>

<p>Comprometimentos remotos são geralmente mais severos que comprometimentos
locais.</p>

<h2>Scripts on-line</h2>

<p>Scripts on-line, especialmente scripts CGI, estão realmente na mesma
classe que servidores de rede &mdash; embora seu servidor de páginas em si
seja seguro, a segurança dos scripts que rodam nele são igualmente importantes.
</p>

<p>Um bug em um script disponível através da rede é tão
sério quanto um bug em um servidor aguardando conexões &mdash; ambos podem
permitir que a máquina seja comprometida da mesma forma.</p>

<h2>Cronjobs e serviços do sistema</h2>

<p>Embora não haja muitos destes, vale a pena olhar os
scripts automáticos, cronjobs, etc, que são incluídos em
pacotes.</p>

<p>Vários coisas de suporte rodam como root por padrão para limpar
arquivos de log, etc.</p>

<p>A exploração bem sucedida de um ataque a link simbólico pode resultar em
um comprometimento local.</p>
