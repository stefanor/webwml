#use wml::debian::translation-check translation="73b976c71b8b4c13c331a478bd9111aa6f64627e"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han encontrado múltiples problemas en cacti, un sistema de monitorización de servidores,
que dan lugar, potencialmente, a ejecución de código SQL o a revelación de información por parte de
usuarios autenticados.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16723">CVE-2019-16723</a>

    <p>Usuarios autenticados pueden sortear las comprobaciones de autorización para visualizar un gráfico
    enviando solicitudes con parámetros local_graph_id modificados.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17357">CVE-2019-17357</a>

    <p>La interfaz de administración de gráficos sanea de forma insuficiente el
    parámetro template_id, dando lugar, potencialmente, a inyección de SQL. Atacantes
    autenticados podrían aprovechar esta vulnerabilidad para ejecutar
    código SQL no autorizado contra la base de datos.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17358">CVE-2019-17358</a>

    <p>La función sanitize_unserialize_selected_items (lib/functions.php)
    sanea de forma insuficiente la entrada proporcionada por el usuario antes de reconstruir los datos serializados que contiene,
    dando lugar, potencialmente, a una reconstrucción insegura de datos serializados que están bajo control del
    usuario. Atacantes autenticados podrían aprovechar esta vulnerabilidad
    para influir en el flujo de control del programa o para provocar corrupción de memoria.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 0.8.8h+ds1-10+deb9u1. Tenga en cuenta que a stretch solo le afectaba
<a href="https://security-tracker.debian.org/tracker/CVE-2018-17358">CVE-2018-17358</a>.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 1.2.2+ds1-2+deb10u2.</p>

<p>Le recomendamos que actualice los paquetes de cacti.</p>

<p>Para información detallada sobre el estado de seguridad de cacti, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/cacti">\
https://security-tracker.debian.org/tracker/cacti</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4604.data"
