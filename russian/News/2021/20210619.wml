#use wml::debian::translation-check translation="ed1b2e9d8349f3e0333668208c8830371bdc453c" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.10</define-tag>
<define-tag release_date>2021-06-19</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о десятом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction apt "Принятие по умолчанию изменения имён выпусков для репозиториев (напр., stable -&gt; oldstable)">
<correction awstats "Исправление удалённого доступа к файлам [CVE-2020-29600 CVE-2020-35176]">
<correction base-files "Обновление /etc/debian_version для редакции 10.10">
<correction berusky2 "Исправление ошибки сегментирования при запуске">
<correction clamav "Новый стабильный выпуск основной ветки разработки; исправление отказа в обслуживании [CVE-2021-1405]">
<correction clevis "Исправление поддержки для TPM, поддерживающих только SHA256">
<correction connman "dnsproxy: проверка длины буферов до выполнения функции memcpy [CVE-2021-33833]">
<correction crmsh "Исправление выполнения произвольного кода [CVE-2020-35459]">
<correction debian-installer "Использование ABI ядра Linux версии 4.19.0-17">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction dnspython "XFR: не пытаться сравнивать с несуществующим значением <q>окончания срока действия</q>">
<correction dput-ng "Исправление аварийной остановки в загрузчике sftp в случае получения EACCES от сервера; обновление кодовых имён; обеспечение работы <q>dcut dm</q> для незагружающих разработчиков; исправление TypeError в коде обработки исключений при http-загрузках; не пытаться и не создавать электронную почту загружающего на основе системного имени узла в файлах .dak-commands">
<correction eterm "Исправление выполнения произвольного кода [CVE-2021-33477]">
<correction exactimage "Исправление сборки с C++11 и OpenEXR 2.5.x">
<correction fig2dev "Исправление переполнения буфера [CVE-2021-3561]; несколько исправлений вывода данных; повторная сборка тестового набора в ходе сборки и при автоматическом тестировании">
<correction fluidsynth "Исправление использования указателей после освобождения памяти [CVE-2021-28421]">
<correction freediameter "Исправление отказа в обслуживании [CVE-2020-6098]">
<correction fwupd "Исправление создания строки SBAT поставщика; прекращение использования dpkg-dev в fwupd.preinst; новая стабильная версия основной ветки разработки">
<correction fwupd-amd64-signed "Синхронизация с fwupd">
<correction fwupd-arm64-signed "Синхронизация с fwupd">
<correction fwupd-armhf-signed "Синхронизация с fwupd">
<correction fwupd-i386-signed "Синхронизация с fwupd">
<correction fwupdate "Улучшение поддержки SBAT">
<correction fwupdate-amd64-signed "Синхронизация с fwupdate">
<correction fwupdate-arm64-signed "Синхронизация с fwupdate">
<correction fwupdate-armhf-signed "Синхронизация с fwupdate">
<correction fwupdate-i386-signed "Синхронизация с fwupdate">
<correction glib2.0 "Исправление нескольких переполнений целых чисел [CVE-2021-27218 CVE-2021-27219]; исправление атаки через символьные ссылки, касающейся file-roller [CVE-2021-28153]">
<correction gnutls28 "Исправление разыменования null-указателя [CVE-2020-24659]; добавление нескольких улучшений по повторному выделению памяти">
<correction golang-github-docker-docker-credential-helpers "Исправление двойного освобождения памяти [CVE-2019-1020014]">
<correction htmldoc "Исправление переполнения буфера [CVE-2019-19630 CVE-2021-20308]">
<correction ipmitool "Исправление переполнения буфера [CVE-2020-5208]">
<correction ircii "Исправление отказа в обслуживании issue [CVE-2021-29376]">
<correction isc-dhcp "Исправление выхода за пределы выделенного буфера памяти [CVE-2021-25217]">
<correction isync "Отклонение <q>забавных</q> имён почтовых ящиков из IMAP LIST/LSUB [CVE-2021-20247]; исправление обработки неожиданного кода ответа APPENDUID [CVE-2021-3578]">
<correction jackson-databind "Исправление раскрытия внешней сущности [CVE-2020-25649] и нескольких связанных с сериализацией проблем [CVE-2020-24616 CVE-2020-24750 CVE-2020-35490 CVE-2020-35491 CVE-2020-35728 CVE-2020-36179 CVE-2020-36180 CVE-2020-36181 CVE-2020-36182 CVE-2020-36183 CVE-2020-36184 CVE-2020-36185 CVE-2020-36186 CVE-2020-36187 CVE-2020-36188 CVE-2020-36189 CVE-2021-20190]">
<correction klibc "malloc: установка errno при ошибке; исправление нескольких переполнений буфера [CVE-2021-31873 CVE-2021-31870 CVE-2021-31872]; cpio: исправление возможной аварийной остановки на 64-битных системах [CVE-2021-31871]; {set,long}jmp [s390x]: сохранение/восстановление правильных регистров FPU">
<correction libbusiness-us-usps-webtools-perl "Обновление до нового API US-USPS">
<correction libgcrypt20 "Исправление слабого шифрования ElGamal с ключами, которые не создаются GnuPG/libgcrypt [CVE-2021-40528]">
<correction libgetdata "Исправление использование указателей после освобождения памяти [CVE-2021-20204]">
<correction libmateweather "Переименование America/Godthab в America/Nuuk как в tzdata">
<correction libxml2 "Исправление чтения за пределами выделенного буфера памяти в xmllint [CVE-2020-24977]; исправление использование указателей после освобождения памяти в xmllint [CVE-2021-3516 CVE-2021-3518]; проверка UTF8 в xmlEncodeEntities [CVE-2021-3517]; передача ошибки в xmlParseElementChildrenContentDeclPriv; исправление экспоненциального раскрытия сущности [CVE-2021-3541]">
<correction liferea "Исправление совместимости с webkit2gtk &gt;= 2.32">
<correction linux "Новый стабильный выпуск основной ветки разработки; увеличение версии ABI до 17; [rt] обновление до 4.19.193-rt81">
<correction linux-latest "Обновление до ABI версии 4.19.0-17">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки; увеличение версии ABI до 17; [rt] обновление до 4.19.193-rt81">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки; увеличение версии ABI до 17; [rt] обновление до 4.19.193-rt81">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки; увеличение версии ABI до 17; [rt] обновление до 4.19.193-rt81">
<correction mariadb-10.3 "Новый выпуск основной ветки разработки; исправления безопасности [CVE-2021-2154 CVE-2021-2166 CVE-2021-27928]; исправление поддержки Innotop; поставка caching_sha2_password.so">
<correction mqtt-client "Исправление отказа в обслуживании [CVE-2019-0222]">
<correction mumble "Исправление удалённого выполнения произвольного кода [CVE-2021-27229]">
<correction mupdf "Исправление использование указателей после освобождения памяти [CVE-2020-16600] и двойного освобождения памяти [CVE-2021-3407]">
<correction nmap "Обновленеи поставляемого списка префиксов MAC">
<correction node-glob-parent "Исправление отказа в обслуживании при обработке регулярного выражения [CVE-2020-28469]">
<correction node-handlebars "Исправление выполнения произвольного кода [CVE-2019-20920 CVE-2021-23369]">
<correction node-hosted-git-info "Исправление отказа в обслуживании при обработке регулярного выражения [CVE-2021-23362]">
<correction node-redis "Исправление отказа в обслуживании при обработке регулярного выражения [CVE-2021-29469]">
<correction node-ws "Исправление отказа в обслуживании при обработке регулярного выражения issue [CVE-2021-32640]">
<correction nvidia-graphics-drivers "Исправление неправильного значения контроля доступа [CVE-2021-1076]">
<correction nvidia-graphics-drivers-legacy-390xx "Исправление неправильного значения контроля доступа [CVE-2021-1076]; исправление ошибки установки для кандидатов на выпуск Linux 5.11">
<correction opendmarc "Исправление переполнения динамической памяти [CVE-2020-12460]">
<correction openvpn "Исправление ошибки <q>illegal client float</q> [CVE-2020-11810]; проверка, чтобы состояние ключа было аутентифицировано до отправки push-ответа [CVE-2020-15078]; увеличение очереди журнала listen() до 32">
<correction php-horde-text-filter "Исправление межсайтового скриптинга [CVE-2021-26929]">
<correction plinth "Использование сессии для проверки приветственного шага первой загрузки">
<correction ruby-websocket-extensions "Исправление отказа в обслуживании [CVE-2020-7663]">
<correction rust-rustyline "Исправление сборки с более новыми версиями rustc">
<correction rxvt-unicode "Отключение экранирующей последовательности ESC G Q [CVE-2021-33477]">
<correction sabnzbdplus "Исправление выполнения произвольного кода [CVE-2020-13124]">
<correction scrollz "Исправление отказа в обслуживании [CVE-2021-29376]">
<correction shim "Новый выпуск основной ветки разработки; добавление поддержки SBAT; исправление повторного выделения памяти для двоичного кода i386; не вызывать QueryVariableInfo() на машинах с EFI 1.10 (например, на старых Intel Mac); исправление обработки ignore_db и user_insecure_mode; добавление сценариев сопровождающего в шаблонные пакеты для управления установкой и удалением fbXXX.efi и mmXXX.efi при установке/удалении пакетов shim-helpers-$arch-signed; завершение работы без вывода ошибок в случае, если установка выполнялась на машину без EFI; не прекращать работу с ошибкой, если вызовы debconf возвращают ошибки">
<correction shim-helpers-amd64-signed "Синхронизация с shim">
<correction shim-helpers-arm64-signed "Синхронизация с shim">
<correction shim-helpers-i386-signed "Синхронизация с shim">
<correction shim-signed "Обновление для новой версии shim; многочисленные исправления ошибок в обработке postinst и postrm; предоставление двоичных файлов без подписи для arm64 (см. NEWS.Debian); завершение работы без вывода ошибок в случае, если установка выполнялась на машину без EFI; не прекращать работу с ошибкой, если вызовы debconf возвращают ошибки; исправление ссылок в документации; сборка с учётом shim-unsigned 15.4-5~deb10u1; добавление явной зависимости из shim-signed в shim-signed-common">
<correction speedtest-cli "Обработка случая, когда опция <q>ignoreids</q> пуста или содержит пустые идентификаторы">
<correction tnef "Исправление чтения за пределами выделенного буфера памяти [CVE-2019-18849]">
<correction uim "libuim-data: копирование поля <q>Breaks</q> из uim-data, что исправляет некоторые сценарии обновления">
<correction user-mode-linux "Повторная сборка с учётом ядра Linux версии 4.19.194-1">
<correction velocity "Исправление потенциального выполнения произвольного кода [CVE-2020-13936]">
<correction wml "Исправление регрессии в обработке Unicode">
<correction xfce4-weather-plugin "Переход на версию 2.0 API met.no">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2021 4848 golang-1.11>
<dsa 2021 4865 docker.io>
<dsa 2021 4873 squid>
<dsa 2021 4874 firefox-esr>
<dsa 2021 4875 openssl>
<dsa 2021 4877 webkit2gtk>
<dsa 2021 4878 pygments>
<dsa 2021 4879 spamassassin>
<dsa 2021 4880 lxml>
<dsa 2021 4881 curl>
<dsa 2021 4882 openjpeg2>
<dsa 2021 4883 underscore>
<dsa 2021 4884 ldb>
<dsa 2021 4885 netty>
<dsa 2021 4886 chromium>
<dsa 2021 4887 lib3mf>
<dsa 2021 4888 xen>
<dsa 2021 4889 mediawiki>
<dsa 2021 4890 ruby-kramdown>
<dsa 2021 4891 tomcat9>
<dsa 2021 4892 python-bleach>
<dsa 2021 4893 xorg-server>
<dsa 2021 4894 php-pear>
<dsa 2021 4895 firefox-esr>
<dsa 2021 4896 wordpress>
<dsa 2021 4898 wpa>
<dsa 2021 4899 openjdk-11-jre-dcevm>
<dsa 2021 4899 openjdk-11>
<dsa 2021 4900 gst-plugins-good1.0>
<dsa 2021 4901 gst-libav1.0>
<dsa 2021 4902 gst-plugins-bad1.0>
<dsa 2021 4903 gst-plugins-base1.0>
<dsa 2021 4904 gst-plugins-ugly1.0>
<dsa 2021 4905 shibboleth-sp>
<dsa 2021 4907 composer>
<dsa 2021 4908 libhibernate3-java>
<dsa 2021 4909 bind9>
<dsa 2021 4910 libimage-exiftool-perl>
<dsa 2021 4912 exim4>
<dsa 2021 4913 hivex>
<dsa 2021 4914 graphviz>
<dsa 2021 4915 postgresql-11>
<dsa 2021 4916 prosody>
<dsa 2021 4918 ruby-rack-cors>
<dsa 2021 4919 lz4>
<dsa 2021 4920 libx11>
<dsa 2021 4921 nginx>
<dsa 2021 4922 hyperkitty>
<dsa 2021 4923 webkit2gtk>
<dsa 2021 4924 squid>
<dsa 2021 4925 firefox-esr>
<dsa 2021 4926 lasso>
<dsa 2021 4928 htmldoc>
<dsa 2021 4929 rails>
<dsa 2021 4930 libwebp>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за причин, на которые мы не можем повлиять:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction sogo-connector "Несовместим с текущими версиями Thunderbird">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
