#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20147">CVE-2018-20147</a>

<p>Des auteurs pourraient modifier les métadonnées pour contourner les
restrictions prévues pour la suppression de fichiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20148">CVE-2018-20148</a>

<p>Des contributeurs pourraient réaliser des attaques par injection d’objet PHP
à l’aide de métadonnées contrefaites dans un appel wp.getMediaItem XMLRPC. Cela
est provoqué par le mauvais traitement de données sérialisées dans phar://·URLs
dans la fonction wp_get_attachment_thumb_file dans wp-includes/post.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20149">CVE-2018-20149</a>

<p>Lorsque le serveur HTTP Apache HTTP est utilisé, des auteurs pourraient
téléverser des fichiers contrefaits contournant les restrictions de type MIME
prévues, conduisant à une faille de script intersite (XSS), comme démontré par un fichier .jpg sans
données JPEG.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20150">CVE-2018-20150</a>

<p>Des URL contrefaits pourraient faire apparaitre des failles de script intersite (XSS) pour certains
cas d’utilisation impliquant des greffons.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20151">CVE-2018-20151</a>

<p>La page d’accueil de l’utilisateur pourrait être lue par un robot
d’indexation de moteur de recherche si une configuration inhabituelle était
choisie. Le moteur de recherche pourrait alors indexer et afficher l’adresse de
courriel de l’utilisateur et (rarement) le mot de passe généré par défaut.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20152">CVE-2018-20152</a>

<p>Des auteurs pourraient contourner des restrictions prévues sur des types
d’article à l’aide d’entrée contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20153">CVE-2018-20153</a>

<p>Des contributeurs pourraient modifier de nouveaux commentaires faits par des
utilisateurs avec de meilleurs privilèges, éventuellement causant une faille
de script intersite (XSS).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 4.1.25+dfsg-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1673.data"
# $Id: $
