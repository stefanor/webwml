#use wml::debian::translation-check translation="1d09518220ed004fd7b6c2b02dca14962cb5c114" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été trouvés dans la bibliothèque d’image
OpenEXR qui pourraient aboutir à un déni de service et, éventuellement,
à l'exécution de code arbitraire lors du traitement de fichiers d’image EXR mal
formés.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.2.0-11+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openexr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openexr, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openexr">https://security-tracker.debian.org/tracker/openexr</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2358.data"
# $Id: $
