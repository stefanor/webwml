#use wml::debian::translation-check translation="b351a304dab615f851f160a904dce3574dd9a743" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans bind9, un serveur de noms de domaine
d’Internet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8622">CVE-2020-8622</a>

<p>Des réponses contrefaites à des requêtes signées TSIG pourraient conduire
à un échec d’assertion, amenant à l’arrêt du serveur. Cela pourrait être réalisé par
des opérateurs malveillants ou des attaquants par prédiction.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8623">CVE-2020-8623</a>

<p>Un échec d’assertion, amenant à l’arrêt du serveur, peut être exploité à l’aide
d’une requête pour une zone signée RSA.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:9.10.3.dfsg.P4-12.3+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2355.data"
# $Id: $
