#use wml::debian::translation-check translation="d24592f1fc49715d0bd42a7f6331e7cbb853d569" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>GnuTLS, une bibliothèque portable de chiffrement, échoue à valider des
chaînes de confiance alternatives dans certaines conditions. En particulier,
cela casse la connexion aux serveurs qui utilisent les certificats de Let's
Encrypt, à partir du 01/10/2021.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 3.5.8-5+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gnutls28.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gnutls28,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gnutls28">\
https://security-tracker.debian.org/tracker/gnutls28</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2759.data"
# $Id: $
