#use wml::debian::translation-check translation="44e7e4f0e4a8478e2a88cb5334267cdb475b5569" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un attaquant distant authentifié peut exécuter du code arbitraire dans
Firebird, un système de base de données relationnelle basé sur le code
d'InterBase 6.0, en exécutant une instruction SQL mal formée. La seule
solution connue est de désactiver le chargement des bibliothèques UDF
externes. Pour cela, la configuration par défaut a été modifiée à
UdfAccess=None. Cela empêchera le module fbudf d’être chargé, mais cela
peut briser d’autres fonctionnalités reposant sur des modules.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
3.0.1.32609.ds4-14+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firebird3.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firebird3.0,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firebird3.0">\
https://security-tracker.debian.org/tracker/firebird3.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2824.data"
# $Id: $
