#use wml::debian::translation-check translation="b4442597fa7513e30707bcac86a9dcacc6cfd407" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>L’annonce DLA-2743-1 a été publiée pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">CVE-2017-5715</a>
affectant amd64-microcode, le micrologiciel pour les processeurs d’AMD.
Cependant, les binaires pour le téléversement n’étaient pas prêts et pas
publiés, empêchant les utilisateurs de mettre à niveau vers une version corrigée.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 3.20181128.1~deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets amd64-microcode.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de amd64-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/amd64-microcode">\
https://security-tracker.debian.org/tracker/amd64-microcode</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2743-2.data"
# $Id: $
