#use wml::debian::translation-check translation="165625c3a669960bb5e1c766db812564b5fd665e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'implémentation du
langage de programmation Perl. Le projet « Common Vulnerabilities and
Exposures » (CVE) identifie les problèmes suivants :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18311">CVE-2018-18311</a>

<p>Jayakrishna Menon et Christophe Hauser ont découvert une vulnérabilité
de dépassement d'entier dans Perl_my_setenv menant à un dépassement de tas
avec une entrée contrôlée par l'attaquant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18312">CVE-2018-18312</a>

<p>Eiichi Tsukata a découvert qu'une expression rationnelle contrefaite
pourrait provoquer un dépassement de tas en écriture durant la compilation,
permettant éventuellement l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18313">CVE-2018-18313</a>

<p>Eiichi Tsukata a découvert qu'une expression rationnelle contrefaite
pourrait provoquer un dépassement de tas en lecture durant la compilation
qui mène à une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18314">CVE-2018-18314</a>

<p>Jakub Wilk a découvert qu'une expression rationnelle contrefaite pour
l'occasion pourrait conduire à un dépassement de tas.</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 5.24.1-3+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets perl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de perl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/perl">\
https://security-tracker.debian.org/tracker/perl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4347.data"
# $Id: $
