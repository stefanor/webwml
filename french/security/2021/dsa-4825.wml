#use wml::debian::translation-check translation="28bc87857803972597b697f1aafdfc05773ea8db" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le serveur de courrier
électronique Dovecot.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24386">CVE-2020-24386</a>

<p>Quand l'hibernation d'IMAP est active, un attaquant (avec une identité
valable pour accéder au serveur de courriel) peut faire en sorte que Dovecot
découvre la structure des répertoires du système de fichiers et accède aux
messages d'autres utilisateurs au moyen de commandes contrefaites pour
l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25275">CVE-2020-25275</a>

<p>Innokentii Sennovskiy a signalé que la distribution et l'analyse de
courriel dans Dovecot peut planter quand la 10 000e partie MIME est de type
message/rfc822 (ou si le parent était au format multipart/digest). Ce
défaut a été introduit par des modifications antérieures pour traiter le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-12100">\
CVE-2020-12100</a>.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1:2.3.4.1-5+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dovecot.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de dovecot, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4825.data"
# $Id: $
