#use wml::debian::template title="Taille d'un miroir"
#use wml::debian::translation-check translation="a7c92aeb803830d49fd98ee440c90a54f3ee1be9" maintainer="Jean-Paul Guillonneau"

<h2>Quelle taille fait l'archive Debian ?</h2>

# (note for the English editors on how to update some of the numbers below)
# dak psql database on ftp-master is 'projectb'
# and there's a copy on merkel
# projectb=> select architecture.arch_string as Architecture,
#            sum(files.size)/1024/1024/1024 as Size
#            from files,binaries,architecture
#            where architecture.id=binaries.architecture
#            and files.id=binaries.file
#            group by architecture.arch_string
#            order by Size;
# projectb=> select sum(size)/1024/1024/1024 from files where
#            filename ~ '.diff.gz$' or filename ~ '.dsc$'
#            or filename ~ '.orig.tar.gz$';
# projectb=> select sum(files.size)/1024/1024/1024
#            from files, binaries, architecture
#            where architecture.id=binaries.architecture
#            and files.id=binaries.file
#            and architecture.arch_string='i386';

# wc -c'ing files inside the debian/ directory might occasionally give
# slightly different results than the SQL queries, but the difference
# is usually negligible -joy

<p>Les chiffres sur cette page reflètent l'état de l'archive, et sont mis
à jour quotidiennement.</p>

<table>
<tr><th>Architecture</th>  <th>Taille en Go</th></tr>
#include "$(ENGLISHDIR)/mirror/size.data"
</table>

<p>Notez que l'archive est en croissance constante ;
« testing » en particulier grossira au fur et à mesure de l’approche de la date
de publication.
Aussi nous ne recommandons pas de réduire la taille d'un miroir en excluant
des distributions spécifiques ; excluez plutôt des architectures
spécifiques, en fonction de leur <a href="https://popcon.debian.org/">popularité</a>.</p>

<h3>Quelle taille fait l'archive des CD Debian ?</h3>

<p>L'archive des CD varie énormément suivant les miroirs &mdash; les fichiers
Jigdo occupent environ de 100 Mo à 150 Mo par architecture, tandis qu'un ensemble
d'images CD ou DVD occupe environ 15 Go, et un peu plus pour les images CD
de mise à niveau, les fichiers Bittorrent, etc.</p>

<p>Pour plus d’informations sur les miroirs de l'archive des CD Debian,
consultez les pages sur la <a href="../CD/mirroring/">création de miroir pour
les images de CD</a>.</p>
