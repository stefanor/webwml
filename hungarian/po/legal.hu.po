msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-01-10 22:51+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/legal.wml:15
msgid "License Information"
msgstr "Licencinformáció"

#: ../../english/template/debian/legal.wml:19
msgid "DLS Index"
msgstr "DLS Index"

#: ../../english/template/debian/legal.wml:23
msgid "DFSG"
msgstr "DFSG"

#: ../../english/template/debian/legal.wml:27
msgid "DFSG FAQ"
msgstr "DFSG GYIK"

#: ../../english/template/debian/legal.wml:31
msgid "Debian-Legal Archive"
msgstr "Debian-Legal Archívum"

#. title string without version, on the form "dls-xxx - license name: status"
#: ../../english/template/debian/legal.wml:49
msgid "%s  &ndash; %s: %s"
msgstr "%s  &ndash; %s: %s"

#. title string with version, on the form "dls-xxx - license name, version: status"
#: ../../english/template/debian/legal.wml:52
msgid "%s  &ndash; %s, Version %s: %s"
msgstr "%s  &ndash; %s, %s: %s Verzió"

#: ../../english/template/debian/legal.wml:59
msgid "Date published"
msgstr "Megjelenés dátuma:"

#: ../../english/template/debian/legal.wml:61
msgid "License"
msgstr "Licensz"

#: ../../english/template/debian/legal.wml:64
msgid "Version"
msgstr "Verzió"

#: ../../english/template/debian/legal.wml:66
msgid "Summary"
msgstr "Összefoglaló"

#: ../../english/template/debian/legal.wml:70
msgid "Justification"
msgstr "Indoklás"

#: ../../english/template/debian/legal.wml:72
msgid "Discussion"
msgstr "Vita"

#: ../../english/template/debian/legal.wml:74
msgid "Original Summary"
msgstr "Eredeti összegzés"

#: ../../english/template/debian/legal.wml:76
msgid ""
"The original summary by <summary-author/> can be found in the <a href="
"\"<summary-url/>\">list archives</a>."
msgstr ""
"<summary-author/> eredeti összeglalója megtalálható a <a href=\"<summary-url/"
">\">lista archívumban</a>."

#: ../../english/template/debian/legal.wml:77
msgid "This summary was prepared by <summary-author/>."
msgstr "Az összefoglalót <summary-author/> készítette."

#: ../../english/template/debian/legal.wml:80
msgid "License text (translated)"
msgstr "Licensz szöveg (fordítás)"

#: ../../english/template/debian/legal.wml:83
msgid "License text"
msgstr "Licensz szöveg"

#: ../../english/template/debian/legal_tags.wml:6
msgid "free"
msgstr "szabad"

#: ../../english/template/debian/legal_tags.wml:7
msgid "non-free"
msgstr "nem-szabad"

#: ../../english/template/debian/legal_tags.wml:8
msgid "not redistributable"
msgstr "nem továbbadható"

#. For the use in headlines, see legal/licenses/byclass.wml
#: ../../english/template/debian/legal_tags.wml:12
msgid "Free"
msgstr "Szabad"

#: ../../english/template/debian/legal_tags.wml:13
msgid "Non-Free"
msgstr "Nem-Szabad"

#: ../../english/template/debian/legal_tags.wml:14
msgid "Not Redistributable"
msgstr "Nem továbbadható"

#: ../../english/template/debian/legal_tags.wml:27
msgid ""
"See the <a href=\"./\">license information</a> page for an overview of the "
"Debian License Summaries (DLS)."
msgstr ""
"A <a href=\"./\">licensz információkat itt találod</a>, ami a Debian License "
"Summaries(DLS) összefoglalja."
