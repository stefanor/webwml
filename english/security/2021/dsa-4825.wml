<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Dovecot email server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24386">CVE-2020-24386</a>

    <p>When imap hibernation is active, an attacker (with valid credentials
    to access the mail server) can cause Dovecot to discover file system
    directory structures and access other users' emails via specially
    crafted commands.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25275">CVE-2020-25275</a>

    <p>Innokentii Sennovskiy reported that the mail delivery and parsing in
    Dovecot can crash when the 10000th MIME part is message/rfc822 (or
    if the parent was multipart/digest). This flaw was introduced by
    earlier changes addressing
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-12100">\
    CVE-2020-12100</a>.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 1:2.3.4.1-5+deb10u5.</p>

<p>We recommend that you upgrade your dovecot packages.</p>

<p>For the detailed security status of dovecot please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4825.data"
# $Id: $
