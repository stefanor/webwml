<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Carlo Arenas discovered a flaw in git, a fast, scalable, distributed
revision control system. With a crafted URL that contains a newline or
empty host, or lacks a scheme, the credential helper machinery can be
fooled into providing credential information that is not appropriate for
the protocol in use and host being contacted.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 1:2.11.0-3+deb9u7.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1:2.20.1-2+deb10u3.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>For the detailed security status of git please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/git">https://security-tracker.debian.org/tracker/git</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4659.data"
# $Id: $
