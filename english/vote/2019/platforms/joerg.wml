#use wml::debian::template title="Platform for Joerg Jaspert" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"

<h1 class="title">DPL Platform 2019</h1>
<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#org5ff9825">1. Who am I?</a></li>
<li><a href="#org3b8c33b">2. Debian activities</a></li>
<li><a href="#orgdfc26f8">3. The Debian Project Leader in 2019</a></li>
<li><a href="#org6096b2d">4. The future</a></li>
<li><a href="#org41c5c7d">5. My part as DPL</a></li>
<li><a href="#org84cbe22">6. Time commitment</a></li>
<li><a href="#org4576cb6">7. What with your delegations?</a></li>
<li><a href="#org2ed253a">8. Rebuttals</a>
<ul>
<li><a href="#org86c63df">8.1. Jonathan Carter</a></li>
<li><a href="#orgcfb0089">8.2. Sam Hartman</a></li>
<li><a href="#orgb90ee39">8.3. Martin Michlmayr</a></li>
<li><a href="#org120099b">8.4. Simon Richter</a></li>
<li><a href="#orgcb30e0c">8.5. Overall</a></li>
</ul>
</li>
</ul>
</div>
</div>

<div id="outline-container-org5ff9825" class="outline-2">
<h2 id="org5ff9825"><span class="section-number-2">1</span> Who am I?</h2>
<div class="outline-text-2" id="text-1">
<p>
I am a Debian Developer since 17 years now, having joined Debian in
April 2002. Real life people think I am <b>THE ANSWER</b> since mid march,
having turned 42 recently. I live right in the middle of Germany, in
the nice medium sized town named Fulda.
</p>
</div>
</div>

<div id="outline-container-org3b8c33b" class="outline-2">
<h2 id="org3b8c33b"><span class="section-number-2">2</span> Debian activities</h2>
<div class="outline-text-2" id="text-2">
<p>
I manned various booth for Debian at a number of events over the
years. There have been multiple LinuxTag, LinuxWorldExpos and Cebits
as well as Chemnitzer LinuxTag, FrosCon and other smaller ones.
Sometimes I just appeared, sometimes I handled the complete orga
needed for it, including creating Debian CD/DVDs to hand out to
visitors as well as organising merchandise. During the first such
event I found (or rather, the upstream author of it found me) my
first package to maintain.
</p>

<p>
I have been an application manager for the NM process since shortly
after I became DD and became Debian account manager assistant in 2004,
full DAM in April 2008.
</p>

<p>
In March 2005 I joined the FTP team as an assistant and became FTP
Master in April 2008.
</p>

<p>
After DebConf5 i joined the the DebConf orga with my main role being
admin of the infrastructure, as back then it was separate to Debian.
Beside the admin stuff I helped with or lead various other teams, say
content or travel sponsorship.
</p>

<p>
I am part of the mirroradmin group, having written the base of the
current master sync scriptset for the mirrors.
</p>

<p>
I am one of the admins for our Debian Planet and am one of the admins
for Debian's gitlab service salsa.debian.org.
</p>

<p>
I am also a staff (NOC) member at OFTC, the IRC network where
irc.debian.org points too.
</p>

<p>
Between August 2007 and February 2018 I have been a board member of
SPI, Software in the Public Interest, with most of the time being
VicePresident and main administrator. SPI is the legal umbrella that
Debian sits in within the United States, and holds the majority of
Debian's assets.
</p>
</div>
</div>

<div id="outline-container-orgdfc26f8" class="outline-2">
<h2 id="orgdfc26f8"><span class="section-number-2">3</span> The Debian Project Leader in 2019</h2>
<div class="outline-text-2" id="text-3">
<p>
It's this time of the year again. Many line up, ready to fire questions
on the -vote list, with only five lined up to answer. It's DPL election
time, long threads to surface at the list. Everyone wants to know how
the next leader is going to lead us into the gloryful future of the
universal OS.
</p>

<p>
How to overcome all the challenges a distribution like ours faces, how
to adapt to the ever faster changing world?
</p>

<p>
How to become more attractive for all the commercial entities that are
currently chosing to ignore Debian but select a derivative?
</p>

<p>
How to become more attractive for developers?
</p>

<p>
How to renew our tools to some more modern workflow? A common wish
being to modernize the BTS (web interface anyone?). Or uploads with a
git push.
</p>

<p>
How to be more diverse? (Look at this years nominees&#x2026;)
</p>

<p>
How to make large-scale changes in Debian simpler?
</p>

<p>
How to deal with the "curl | sudo bash" mentality of newer languages
and their environment? How to, usefully, include those languages?
</p>

<p>
And I am sure, many many more.
</p>
</div>
</div>

<div id="outline-container-org6096b2d" class="outline-2">
<h2 id="org6096b2d"><span class="section-number-2">4</span> The future</h2>
<div class="outline-text-2" id="text-4">
<p>
I am not really good in predicting the future, but one thing I am
willing to put a bet on is that Debian will continue to be relevant
for the community. We have many users, directly or indirectly via
derivatives. They continue to need a stable, predictable and simply
working system, which we are good at delivering.
</p>

<p>
We should not relax and accept just being a building ground for others
though, we should look why people chose something different than
Debian. And see if we can enhance Debian to provide the features,
while balancing it with our current users. And yes, that includes
users that are building on top of us.
</p>

<p>
We need to ensure to cater to the millions of users we have. They
range from those who want a really stable system and loath change to
those that even find our unstable distribution to slow to change.
</p>

<p>
Thats not an impossible task, we have some of the most brilliant
people working with the project. We are the experts in making sure our
software integrates. And we need to make sure that we keep those
properties. And extend them to the new challenges.
</p>

<p>
And some of those challenges are big. The current hypes on the current
"perfect way of doing sysadmin/devops/&#x2026;" are seldom based on full
distributions.
</p>
</div>
</div>

<div id="outline-container-org41c5c7d" class="outline-2">
<h2 id="org41c5c7d"><span class="section-number-2">5</span> My part as DPL</h2>
<div class="outline-text-2" id="text-5">
<p>
As the DPL is not the lead of actual technical development, it is not
for the DPL to find technical solutions for the challenges we face. Or
to find the perfect answers to the series of questions (and all the others
that are out there) I put above.
</p>

<p>
But the DPL ought to enable other project members to do so, and that
is one major part I will focus on. Many of the above points are
nothing that can be implemented in short terms like a year, but at
least one can start on them. Some may already get worked on, then it
is important to continue work and keep people engaged and motivated.
</p>

<p>
The DPL should work removing blockers and let developers get on with
their jobs.
</p>

<p>
It's also the job of the DPL to listen. And be guided by the
community.
</p>

<p>
Debian does not only consist of packaging and similar technical
activities, we have a lot of other contributions that are equally
important. We need to encourage more people who aren't well versed in
the technical work to contribute in various non-technical ways. That
can range from writing documentation, translations, help with the
website, design work, help users with their problems or representing
Debian at events. Or organizing such events, like Bug Squashing
Parties or local "miniconf" gatherings. All of that is as important as
the packagers work.
</p>

<p>
I will work with anyone who wants to improve processes within Debian
to identify non-trivial bottlenecks, to reduce our complexity and to
improve communication between teams, where needed.
</p>

<p>
I intend to continue the transparent ways from Chris (and other former
DPLs) with regular activity reports, though my style naturally will be
different.
</p>

<p>
Also, as we are a really big project, there will be areas where I
simply not know enough (or anything) about. I'm not afraid asking for
help.
</p>
</div>
</div>

<div id="outline-container-org84cbe22" class="outline-2">
<h2 id="org84cbe22"><span class="section-number-2">6</span> Time commitment</h2>
<div class="outline-text-2" id="text-6">
<p>
The DPL job is one that takes a lot of time and energy. Having a
fulltime job and a family with 2 kids does not make it easier. As such
I discussed it with all involved and got full support for my
nomination.
</p>

<p>
To be a bit more precise: My company calculates with a full 45
workdays that I spend on DPL duties (out of the approximately 250
workdays during a DPL term). Additionally to my usual vacation days.
</p>

<p>
The time I take will depend on the actual DPL tasks and can range from
just hours a day to full absence when traveling to some event.
</p>

<p>
That is just what I take out of work time, the actual time I can spend
on DPL duties will be higher, but I am not going to put up a definite
number. It will adjust to what is needed, when it is needed.
</p>
</div>
</div>

<div id="outline-container-org4576cb6" class="outline-2">
<h2 id="org4576cb6"><span class="section-number-2">7</span> What with your delegations?</h2>
<div class="outline-text-2" id="text-7">
<p>
Lets start with the most important one in this context, DAM. The
constitution is simple there. $8.1.2 is specific for this. It says:
</p>

<blockquote>
<p>
The Project Leader's Delegates:
</p>

<p>
[&#x2026;]
</p>

<p>
may make certain decisions which the Leader may not make directly,
including approving or expelling Developers or designating people
as Developers who do not maintain packages. This is to avoid
concentration of power, particularly over membership as a
Developer, in the hands of the Project Leader.
</p>
</blockquote>
<p>
The simple takeaway here is that, should I get elected, I will resign
from DAM.
</p>

<p>
Besides the above quote the constitution only forbids the secretary
and the chair of the CTTE to be DPL, so I do not see any conflict with
keeping my other delegation, FTPMaster. That is, until the moment I
would need to update it, then I would need to resign from that, as a
DPL may not delegate to themself.
</p>
</div>
</div>

<div id="outline-container-org2ed253a" class="outline-2">
<h2 id="org2ed253a"><span class="section-number-2">8</span> Rebuttals</h2>
<div class="outline-text-2" id="text-8">
</div>
<div id="outline-container-org86c63df" class="outline-3">
<h3 id="org86c63df"><span class="section-number-3">8.1</span> Jonathan Carter</h3>
<div class="outline-text-3" id="text-8-1">
<p>
I agree with him on not putting out big goals that one must achieve
during the term. His goals and execution plans mostly read ok, though
I find it problematic to already announce times where he won't work as
DPL.
</p>

<p>
I don't think the DPL position should be treated that way.
</p>
</div>
</div>

<div id="outline-container-orgcfb0089" class="outline-3">
<h3 id="orgcfb0089"><span class="section-number-3">8.2</span> Sam Hartman</h3>
<div class="outline-text-3" id="text-8-2">
<p>
His main point appears to be about a new communication style keeping
feelings of everybody in mind, keep processes short lived and keep
Debian fun for the project members. From what I know of him I am sure
he will be able to implement a lot of this. Actually, should I really
win, I would want to work together with him on that point, as it is an
important point and he certainly is better at it than I am.
</p>

<p>
The rest of his platform appears to be to keep the usual day to day
work of DPL ongoing.
</p>
</div>
</div>

<div id="outline-container-orgb90ee39" class="outline-3">
<h3 id="orgb90ee39"><span class="section-number-3">8.3</span> Martin Michlmayr</h3>
<div class="outline-text-3" id="text-8-3">
<p>
His platform starts out with him admitting he wanted to resign but
now, well, lets take another stint as DPL. This is followed by a long
series of points listing what he thinks is wrong in Debian.
</p>

<p>
One of the more controversial points and one that will probably be the
most controversial one if he gets elected is the part about Debian
needing more paid Developers and more company involvment to continue
to exist.
</p>

<p>
While he has experience as DPL, I think that is from quite long ago
now and a lot happened so Debian changed signifiantly since then. I
also feel starting out negatively is the wrong approach.
</p>
</div>
</div>

<div id="outline-container-org120099b" class="outline-3">
<h3 id="org120099b"><span class="section-number-3">8.4</span> Simon Richter</h3>
<div class="outline-text-3" id="text-8-4">
<p>
As there is still no platform and not a single mail to any of the threads
since nomination, no rebuttal is possible. Wish him the best, whatever
he is up to now.
</p>
</div>
</div>

<div id="outline-container-orgcb30e0c" class="outline-3">
<h3 id="orgcb30e0c"><span class="section-number-3">8.5</span> Overall</h3>
<div class="outline-text-3" id="text-8-5">
<p>
In general all available platforms have good points and all candidates
with one are certainly not bad for the position, my personal
preference for the vote outcome, aside me winning, ranks Sam above
Jonathan followed by Martin.
</p>

<p>
Whatever the outcome will be, I wish everyone luck and am sure the new
DPL, whoever it will be, will do a good job for Debian.
</p>
</div>
</div>
</div>
<div id="postamble" class="status">
<p class="author">Author: Joerg Jaspert</p>
<p class="date">Created: 2019-03-24 Sun 10:14</p>
</div>
