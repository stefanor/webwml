<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0224">CVE-2014-0224</a>

    <p>This update updates the upstream fix for <a href="https://security-tracker.debian.org/tracker/CVE-2014-0224">CVE-2014-0224</a> to address
    problems with renegotiation under some conditions.</p>

    <p>original text:
    KIKUCHI Masashi discovered that carefully crafted handshakes can
    force the use of weak keys, resulting in potential man-in-the-middle
    attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2012-4929">CVE-2012-4929</a>

    <p>ZLIB compression is now disabled by default.  If you need
    to re-enable it for some reason, you can set the environment
    variable OPENSSL_NO_DEFAULT_ZLIB.</p></li>

</ul>

<p>It's important that you upgrade the libssl0.9.8 package and not
just the openssl package.</p>

<p>All applications linked to openssl need to be restarted. You can
use the tool checkrestart from the package debian-goodies to
detect affected programs or reboot your system.</p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in openssl version 0.9.8o-4squeeze16</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2014/dla-0008.data"
# $Id: $
