<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the implementation of UntypedObjectDeserializer in
jackson-databind, a fast and powerful JSON library for Java, was prone to a
denial of service attack when deeply nested object and array values were
processed.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.8.6-1+deb9u10.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>For the detailed security status of jackson-databind please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/jackson-databind">https://security-tracker.debian.org/tracker/jackson-databind</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2990.data"
# $Id: $
