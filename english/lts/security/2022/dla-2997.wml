<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In ecdsautils, a collection of ECDSA elliptic curve cryptography command
line tools, an improper verification of cryptographic signatures was
detected. A signature consisting only of zeroes is always considered
valid, making it trivial to forge signatures.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24884">CVE-2022-24884</a></li>
</ul>

<p>For Debian 9 stretch, this problem has been fixed in version
0.3.2+git20151018-2+deb9u1.</p>

<p>We recommend that you upgrade your ecdsautils packages.</p>

<p>For the detailed security status of ecdsautils please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ecdsautils">https://security-tracker.debian.org/tracker/ecdsautils</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2997.data"
# $Id: $
