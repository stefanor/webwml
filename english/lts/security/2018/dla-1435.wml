<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The dns-root-data update to 2017072601~deb8u2 broke dnsmasq's
init script, making dnsmasq no longer start when dns-root-data
was installed.</p>

<p>This update fixes dnsmasq's parsing of dns-root-data.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.72-3+deb8u3.</p>

<p>We recommend that you upgrade your dnsmasq packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1435.data"
# $Id: $
