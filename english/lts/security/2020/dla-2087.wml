<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two vulnerabilities have recently been discovered in the stream-tcp code
of the intrusion detection and prevention tool Suricata.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18625">CVE-2019-18625</a>

    <p>It was possible to bypass/evade any tcp based signature by faking a
    closed TCP session using an evil server. After the TCP SYN packet, it
    was possible to inject a RST ACK and a FIN ACK packet with a bad TCP
    Timestamp option. The client would have ignored the RST ACK and the
    FIN ACK packets because of the bad TCP Timestamp option.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18792">CVE-2019-18792</a>

    <p>It was possible to bypass/evade any tcp based signature by
    overlapping a TCP segment with a fake FIN packet. The fake FIN packet
    had to be injected just before the PUSH ACK packet we wanted to
    bypass. The PUSH ACK packet (containing the data) would have been
    ignored by Suricata because it would have overlapped the FIN packet
    (the sequence and ack number are identical in the two packets). The
    client would have ignored the fake FIN packet because the ACK flag
    would not have been set.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.0.7-2+deb8u5.</p>

<p>We recommend that you upgrade your suricata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2087.data"
# $Id: $
