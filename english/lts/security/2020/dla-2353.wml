<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in bacula, a network backup service.
By sending oversized digest strings a malicious client can cause a heap
overflow in the director's memory which results in a denial of service.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
7.4.4+dfsg-6+deb9u2.</p>

<p>We recommend that you upgrade your bacula packages.</p>

<p>For the detailed security status of bacula please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/bacula">https://security-tracker.debian.org/tracker/bacula</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2353.data"
# $Id: $
