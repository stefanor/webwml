<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The following CVEs were reported against dojo:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-5258">CVE-2020-5258</a>

    <p>In affected versions of dojo, the deepCopy method is vulnerable
    to Prototype Pollution. An attacker could manipulate these attributes
    to overwrite, or pollute, a JavaScript application object prototype
    of the base object by injecting other values.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-5259">CVE-2020-5259</a>

    <p>The Dojox jQuery wrapper jqMix mixin method is vulnerable to
    Prototype Pollution. An attacker could manipulate these attributes
    to overwrite, or pollute, a JavaScript application object prototype
    of the base object by injecting other values.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.10.2+dfsg-1+deb8u3.</p>

<p>We recommend that you upgrade your dojo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2139.data"
# $Id: $
