<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or have other
impacts.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9888">CVE-2014-9888</a>

    <p>Russell King found that on ARM systems, memory allocated for DMA
    buffers was mapped with executable permission.  This made it
    easier to exploit other vulnerabilities in the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9895">CVE-2014-9895</a>

    <p>Dan Carpenter found that the MEDIA_IOC_ENUM_LINKS ioctl on media
    devices resulted in an information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6786">CVE-2016-6786</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-6787">CVE-2016-6787</a>

    <p>It was discovered that the performance events subsystem does not
    properly manage locks during certain migrations, allowing a local
    attacker to escalate privileges.  This can be mitigated by
    disabling unprivileged use of performance events:
    sysctl kernel.perf_event_paranoid=3</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8405">CVE-2016-8405</a>

    <p>Peter Pi of Trend Micro discovered that the frame buffer video
    subsystem does not properly check bounds while copying color maps to
    userspace, causing a heap buffer out-of-bounds read, leading to
    information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5549">CVE-2017-5549</a>

    <p>It was discovered that the KLSI KL5KUSB105 serial USB device
    driver could log the contents of uninitialised kernel memory,
    resulting in an information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6001">CVE-2017-6001</a>

    <p>Di Shen discovered a race condition between concurrent calls to
    the performance events subsystem, allowing a local attacker to
    escalate privileges. This flaw exists because of an incomplete fix
    of <a href="https://security-tracker.debian.org/tracker/CVE-2016-6786">CVE-2016-6786</a>.  
    This can be mitigated by disabling unprivileged
    use of performance events: sysctl kernel.perf_event_paranoid=3</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6074">CVE-2017-6074</a>

    <p>Andrey Konovalov discovered a use-after-free vulnerability in the
    DCCP networking code, which could result in denial of service or
    local privilege escalation.  On systems that do not already have
    the dccp module loaded, this can be mitigated by disabling it:
    echo &gt;&gt; /etc/modprobe.d/disable-dccp.conf install dccp false</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.84-2.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.16.39-1+deb8u1 or earlier.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-833.data"
# $Id: $
