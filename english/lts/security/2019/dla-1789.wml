<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update ships updated CPU microcode for most types of Intel CPUs. It
provides microcode support for the kernel to implement mitigations for
the MSBDS, MFBDS, MLPDS and MDSUM hardware vulnerabilities.</p>

<p>To fully resolve these vulnerabilities it is also necessary to update
the Linux kernel packages, which is the subject of DLA-1787-1.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.20190514.1~deb8u1 of the intel-microcode package, and also by the
Linux kernel updates described in DLA-1787-1.</p>

<p>We recommend that you upgrade your intel-microcode packages, and Linux
kernel packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be found
at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For the detailed security status of intel-microcode please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1789.data"
# $Id: $
