#use wml::debian::template title="데비안 전세계 미러 사이트" BARETITLE=true
#use wml::debian::translation-check translation="c1ef6c3811be792e129656bbfcf09866d9880eb5" maintainer="Sebul" 

<p>데비안은 인터넷의 수백 서버에서 배포(<em>미러</em>)됩니다.
가까운 서버를 쓰면 다운로드 속도가 빨라질 것이고, 중심 서버 그리고 인터넷 전체의 부하와 줄일 겁니다.
</p>

<p class="centerblock">
데비안 미러는 여러 나라에 존재하며, 일부 국가에서는 <code>ftp.&lt;country&gt;.debian.org</code> alias를 추가했습니다.
이 alias는 일반적으로 정기적으로 빠르게 동기화하고 데비안의 모든 아키텍처를 전달하는 미러를 가리킵니다. 
데비안 아카이브는 서버의 /debian 위치에서 HTTP를 통해 항상 사용할 수 있습니다.
</p>

<p class="centerblock">
다른 <strong>미러 사이트</strong>에는 공간 제한으로 인해 미러링하는 항목에 제한이 있을 수 있습니다. 
단지 사이트가 국가의 <code>ftp.&lt;나라&gt;.debian.org</code> 가 아니라고 해서 
<code>ftp.&lt;나라&gt;.debian.org</code>보다 느리거나 최신 상태임을 뜻하지는 않습니다.
사실 사용자로서 사용자의 아키텍처를 전달하고 더 가까운 곳에 있는 미러가, 더 멀리 있는 다른 미러보다 훨씬 빠릅니다.
</p>

<p>빠른 다운로드가 가능하도록 여러분에게 가까운 사이트를 쓰세요.
<a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a> 프로그램을 써서 대기시간이 가장 짧은 사이트를 결정할 수 있습니다.
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> 또는
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a> 같은 다운로드 프로그램을 써서 처리량이 가장 많은 사이트를 결정하세요.
지리적 근접성이 때론 어떤 기계가 여러분에게 최상의 서비스를 제공하는지에 가장 중요한 요소는 아닙니다.
</p>

<p>시스템이 많이 움직인다면 글로벌 <abbr title="Content Delivery Network">CDN</abbr>이 지원하는 "미러"를 통해 
최상의 서비스를 받을 수 있습니다.
데비안 프로젝트는 이러한 목적을 위해 deb.debian.org를 유지하며, 
당신은 이것을 적절한 source.list에서 사용할 수 있습니다. 
자세한 내용은 <a href="http://deb.debian.org/"> 웹사이트를 참조</a>하세요.

<p>
데비안 미러에 대해 여러분이 알기 바라는 모든 것:
<url "https://www.debian.org/mirror/">.
</p>

<h2 class="center">나라별 데비안 미러</h2>

<table border="0" class="center">
<tr>
  <th>나라</th>
  <th>사이트</th>
  <th>아키텍처</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">데비안 아카이브의 미러 목록</h2>

<table border="0" class="center">
<tr>
  <th>Host 이름</th>
  <th>HTTP</th>
  <th>아키텍처</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
