#use wml::debian::translation-check translation="156615cc19b61bffcfb93a5ff5e5e300fcbc9492"
<define-tag pagetitle>更新 Debian 10：10.8 版本已发布</define-tag>
<define-tag release_date>2021-02-06</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian 项目很高兴地宣布稳定发行版 Debian <release>（代号 <q><codename></q>）
的第八次更新。 
此节点版本主要针对安全问题进行了修复，并针对严重问题进行了一些调整。
安全警告已单独发布，并可引用。
</p>

<p>请注意此节点版本并不构成 Debian <release> 的新版本，仅更新了其中的一部分软件包。
没有必要换掉旧的 <q><codename></q> 安装媒介。
在安装后，可以使用最新的 Debian 镜像将软件包升级到当前版本。</p>

<p>经常从 security.debian.org 安装更新的用户无需更新很多软件包，
大多数这样的更新都包含在节点版本中。</p>

<p>新的安装镜像将很快在常规位置提供。</p>

<p>将包管理器指向 Debian 的众多 HTTP 镜像之一，可以将现有的安装升级到此版本。
访问以下网址以获得所有镜像的列表：</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>其他错误修复</h2>

<p>此稳定版更新为以下软件包作了一些重要的修复：</p>

<table border=0>
<tr><th>软件包</th>               <th>原因</th></tr>
<correction atftp "Fix denial of service issue [CVE-2020-6097]">
<correction base-files "Update /etc/debian_version for the 10.8 point release">
<correction ca-certificates "Update Mozilla CA bundle to 2.40, blacklist expired <q>AddTrust External Root</q>">
<correction cacti "Fix SQL injection issue [CVE-2020-35701] and stored XSS issue">
<correction cairo "Fix mask usage in image-compositor [CVE-2020-35492]">
<correction choose-mirror "Update mirror list">
<correction cjson "Fix infinite loop in cJSON_Minify">
<correction clevis "Fix initramfs creation; clevis-dracut: Trigger initramfs creation upon installation">
<correction cyrus-imapd "Fix version comparison in cron script">
<correction debian-edu-config "Move host keytabs cleanup code out of gosa-modify-host into a standalone script, reducing LDAP calls to a single query">
<correction debian-installer "Use 4.19.0-14 Linux kernel ABI; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-installer-utils "Support partitions on USB UAS devices">
<correction device-tree-compiler "Fix segfault on <q>dtc -I fs /proc/device-tree</q>">
<correction didjvu "Add missing build-dependency on tzdata">
<correction dovecot "Fix crash when searching mailboxes containing malformed MIME messages">
<correction dpdk "New upstream stable release">
<correction edk2 "CryptoPkg/BaseCryptLib: fix NULL dereference [CVE-2019-14584]">
<correction emacs "Don't crash with OpenPGP User IDs with no e-mail address">
<correction fcitx "Fix input method support in Flatpaks">
<correction file "Increase name recursion depth to 50 by default">
<correction geoclue-2.0 "Check the maximum allowed accuracy level even for system applications; make the Mozilla API key configurable and use a Debian-specific key by default; fix display of the usage indicator">
<correction gnutls28 "Fix test suite error caused by expired certificate">
<correction grub2 "When upgrading grub-pc noninteractively, bail out if grub-install fails; explicitly check whether the target device exists before running grub-install; grub-install: Add backup and restore; don't call grub-install on fresh install of grub-pc">
<correction highlight.js "Fix prototype pollution [CVE-2020-26237]">
<correction intel-microcode "Update various microcode">
<correction iproute2 "Fix bugs in JSON output; fix race condition that DOSes the system when using ip netns add at boot">
<correction irssi-plugin-xmpp "Do not trigger the irssi core connect timeout prematurely, thus fixing STARTTLS connections">
<correction libdatetime-timezone-perl "Update for new tzdata version">
<correction libdbd-csv-perl "Fix test failure with libdbi-perl 1.642-1+deb10u2">
<correction libdbi-perl "Security fix [CVE-2014-10402]">
<correction libmaxminddb "Fix heap-based buffer over-read [CVE-2020-28241]">
<correction lttng-modules "Fix build on kernel versions &gt;= 4.19.0-10">
<correction m2crypto "Fix compatibility with OpenSSL 1.1.1i and newer">
<correction mini-buildd "builder.py: sbuild call: set '--no-arch-all' explicitly">
<correction net-snmp "snmpd: Add cacheTime and execType flags to EXTEND-MIB">
<correction node-ini "Do not allow invalid hazardous string as section name [CVE-2020-7788]">
<correction node-y18n "Fix prototype pollution issue [CVE-2020-7774]">
<correction nvidia-graphics-drivers "New upstream release; fix possible denial of service and information disclosure [CVE-2021-1056]">
<correction nvidia-graphics-drivers-legacy-390xx "New upstream release; fix possible denial of service and information disclosure [CVE-2021-1056]">
<correction pdns "Security fixes [CVE-2019-10203 CVE-2020-17482]">
<correction pepperflashplugin-nonfree "Turn into a dummy package taking care of removing the previously installed plugin (no longer functional nor supported)">
<correction pngcheck "Fix buffer overflow [CVE-2020-27818]">
<correction postgresql-11 "New upstream stable release; security fixes [CVE-2020-25694 CVE-2020-25695 CVE-2020-25696]">
<correction postsrsd "Ensure timestamp tags aren't too long before trying to decode them [CVE-2020-35573]">
<correction python-bottle "Stop allowing <q>;</q> as a query-string separator [CVE-2020-28473]">
<correction python-certbot "Automatically use ACMEv2 API for renewals, to avoid issues with ACMEv1 API removal">
<correction qxmpp "Fix potential SEGFAULT on connection error">
<correction silx "python(3)-silx: Add dependency on python(3)-scipy">
<correction slirp "Fix buffer overflows [CVE-2020-7039 CVE-2020-8608]">
<correction steam "New upstream release">
<correction systemd "journal: do not trigger assertion when journal_file_close() is passed NULL">
<correction tang "Avoid race condition between keygen and update">
<correction tzdata "New upstream release; update included timezone data">
<correction unzip "Apply further fixes for CVE-2019-13232">
<correction wireshark "Fix various crashes, infinite loops and memory leaks [CVE-2019-16319 CVE-2019-19553 CVE-2020-11647 CVE-2020-13164 CVE-2020-15466 CVE-2020-25862 CVE-2020-25863 CVE-2020-26418 CVE-2020-26421 CVE-2020-26575 CVE-2020-28030 CVE-2020-7045 CVE-2020-9428 CVE-2020-9430 CVE-2020-9431]">
</table>


<h2>安全更新</h2>


<p>此修订版在稳定版本中添加了以下安全更新。 
安全团队已经为每个更新发布了相关建议：</p>

<table border=0>
<tr><th>公告序号</th>  <th>软件包</th></tr>
<dsa 2020 4797 webkit2gtk>
<dsa 2020 4801 brotli>
<dsa 2020 4802 thunderbird>
<dsa 2020 4803 xorg-server>
<dsa 2020 4804 xen>
<dsa 2020 4805 trafficserver>
<dsa 2020 4806 minidlna>
<dsa 2020 4807 openssl>
<dsa 2020 4808 apt>
<dsa 2020 4809 python-apt>
<dsa 2020 4810 lxml>
<dsa 2020 4811 libxstream-java>
<dsa 2020 4812 xen>
<dsa 2020 4813 firefox-esr>
<dsa 2020 4814 xerces-c>
<dsa 2020 4815 thunderbird>
<dsa 2020 4816 mediawiki>
<dsa 2020 4817 php-pear>
<dsa 2020 4818 sympa>
<dsa 2020 4819 kitty>
<dsa 2020 4820 horizon>
<dsa 2020 4821 roundcube>
<dsa 2021 4822 p11-kit>
<dsa 2021 4823 influxdb>
<dsa 2021 4824 chromium>
<dsa 2021 4825 dovecot>
<dsa 2021 4827 firefox-esr>
<dsa 2021 4828 libxstream-java>
<dsa 2021 4829 coturn>
<dsa 2021 4830 flatpak>
<dsa 2021 4831 ruby-redcarpet>
<dsa 2021 4832 chromium>
<dsa 2021 4833 gst-plugins-bad1.0>
<dsa 2021 4834 vlc>
<dsa 2021 4835 tomcat9>
<dsa 2021 4837 salt>
<dsa 2021 4838 mutt>
<dsa 2021 4839 sudo>
<dsa 2021 4840 firefox-esr>
<dsa 2021 4841 slurm-llnl>
<dsa 2021 4843 linux-latest>
<dsa 2021 4843 linux-signed-amd64>
<dsa 2021 4843 linux-signed-arm64>
<dsa 2021 4843 linux-signed-i386>
<dsa 2021 4843 linux>
</table>


<h2>移除的软件包</h2>

<p>由于现有条件限制，我们移除了下列软件包：</p>

<table border=0>
<tr><th>软件包</th>               <th>原因</th></tr>
<correction compactheader "Incompatible with current Thunderbird versions">

</table>

<h2>Debian 安装器</h2>
<p>安装程序已更新，现已包含节点版本中整合到稳定版中的修复程序。</p>

<h2>相关网页链接</h2>

<p>随此修订版更改的包的完整列表：</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>当前稳定版分发：</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>稳定版分发的计划更新：</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>稳定版分发信息（发行说明、勘误表等）：</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>安全公告与信息：</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>关于 Debian</h2>

<p>Debian 是自由软件开发者们的协会，他们自愿贡献时间参与开发，
致力于创建完全自由的操作系统 Debian。</p>

<h2>联系我们</h2>

<p>请访问 Debian 网站 <a href="$(HOME)/">https://www.debian.org/</a>，
发送邮件到 &lt;press@debian.org&gt; ，
或通过 &lt;debian-release@lists.debian.org&gt; 联系稳定版发行团队
以了解更多信息。</p>
