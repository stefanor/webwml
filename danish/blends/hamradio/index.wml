#use wml::debian::blend title="Debian Hamradio Pure Blend" NOHEADER="true" BLENDHOME="true"
#use wml::debian::recent_list
#use wml::debian::blends::hamradio
#use wml::debian::translation-check translation="9bc4ecf1abce08efe4132833b3d0ba344ca3ff10"
# $Id$

<div id="splash">
    <h1 id="hamradio">Debian Hamradio Pure Blend</h1>
</div>

<p><b>Debian Hamradio Pure Blend</b> er et projekt fra 
<a href="https://wiki.debian.org/DebianHams/">Debian Hamradio Maintainers 
Team</a>, der samarbejder om vedligeholdse af pakker med relation til 
amatørradio i Debian.  Hver <a href="https://blends.debian.org/">Pure Blend</a> 
er en delmængde af Debian, som fra starten er opsat til at understøtte en 
bestemt målgruppe.  Denne blend har til formål at understøtte radioamatørers 
behov.</p>

<p><a href="./about">Læs mere&hellip;</a></p>

<div id="hometoc">
<ul id="hometoc-cola">
  <li><a href="./about">Om denne blend</a></li>
  <li><a href="./News/">Nyhedsarkiv</a></li>
  <li><a href="./contact">Kontakt os</a></li>
</ul>
<ul id="hometoc-colb">
  <li><a href="./get/">Få fat i blend'en</a>
  <ul>
    <li><a href="./get/metapackages">Anvendelse af metapakkerne</a></li>
  </ul></li>
</ul>
<ul id="hometoc-colc">
  <li><a href="./docs">Dokumentation</a>
  <ul>
    <li><a href="https://wiki.debian.org/DebianHams/Handbook">Debian Hamradio Handbook</a></li>
  </ul></li>
  <li><a href="./support">Support</a></li>
</ul>
<ul id="hometoc-cold">
  <li><a href="./dev">Udvikling</a>
    <ul>
      <li><a href="https://wiki.debian.org/DebianHams">Hamradio Maintainers Team</a></li>
      <li><a href="https://www.debian.org/doc/user-manuals#hamradio-maintguide">Hamradio Maintainers Guide</a></li>
    </ul>
  </li>
</ul>
<ul id="hometoc-cole">
  <li><a href="https://twitter.com/DebianHamradio"><img src="Pics/twitter.gif" alt="Twitter" width="80" height="15" /></a></li>
</ul>
</div>

<h2>Kom i gang</h2>

<ul>
    <li>Hvis du allerede har installeret Debian, så se 
	<a href="./get/metapackages">listen over metapakker</a> for at finde 
	amatørradiosoftware, som du kan installere.</li>
    <li>For at få hjælp til at anvende amatørradiosoftware i Debian, kan du 
	benytte en af vores <a href="./support">supportkanaler</a>.</li>
</ul>

<h2>Nyheder</h2>

<p><:= get_recent_list('News/$(CUR_YEAR)', '6',
'$(ENGLISHDIR)/blends/hamradio', '', '\d+\w*' ) :>
</p>
