# distrib webwml Catalan template.
# Copyright (C) 2004, 2005 Free Software Foundation, Inc.
# Guillem Jover <guillem@debian.org>, 2004-2008, 2011, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2019-02-22 10:52+0100\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Paraula clau"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Mostra"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "camins acabats amb la paraula clau"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "paquets que contenen fitxers anomenats d'aquesta manera"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr "paquets que contenen fitxers on els seus noms contenen la paraula clau"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Distribució"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr "experimental"

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "unstable"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "testing"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "stable"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "oldstable"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Arquitectura"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr "qualsevol"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "Cerca"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "Esborra"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Cerca a"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Només noms de paquet"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "Descripció"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "Noms de paquet font"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "Només mostra concordances exactes"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Secció"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "PC de 64 bits (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr "ARM de 64 bits (AArch64)"

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr "ARM amb EABI (armel)"

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr "ARM amb ABI de coma flotant de maquinari (armhf)"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd PC de 32 bits (i386)"

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr "PC de 32 bits (i386)"

#: ../../english/releases/arches.data:17
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD PC de 32 bits (i386)"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD PC de 64 bits (amd64)"

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr "MIPS (terminació gran)"

#: ../../english/releases/arches.data:22
msgid "64-bit MIPS (little endian)"
msgstr "MIPS de 64 bits (terminació petita)"

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr "MIPS (terminació petita)"

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr "Processador POWER"

#: ../../english/releases/arches.data:26
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr "RISC-V de 64 bits amb terminació petita (riscv64)"

#: ../../english/releases/arches.data:27
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:28
msgid "IBM System z"
msgstr "Systema z d&#39;IBM"

#: ../../english/releases/arches.data:29
msgid "SPARC"
msgstr "SPARC"

#~ msgid "AMD64"
#~ msgstr "AMD64"

#~ msgid "Allow searching on subwords"
#~ msgstr "Permet la cerca en subparaules"

#~ msgid "Case sensitive"
#~ msgstr "Distingeix entre majúscules i minúscules"

#~ msgid "HP PA/RISC"
#~ msgstr "HP PA/RISC"

#~ msgid "Hurd (i386)"
#~ msgstr "Hurd (i386)"

#~ msgid "Intel IA-64"
#~ msgstr "Intel IA-64"

#~ msgid "Intel x86"
#~ msgstr "Intel x86"

#~ msgid "MIPS"
#~ msgstr "MIPS"

#~ msgid "MIPS (DEC)"
#~ msgstr "MIPS (DEC)"

#~ msgid "Search case sensitive"
#~ msgstr "Cerca tenint en compte la diferència entre majúscules i minúscules"

#~ msgid "all files in this package"
#~ msgstr "tots els fitxers d'aquest paquet"

#~ msgid "kFreeBSD (AMD64)"
#~ msgstr "kFreeBSD (AMD64)"

#~ msgid "kFreeBSD (Intel x86)"
#~ msgstr "kFreeBSD (Intel x86)"

#~ msgid "no"
#~ msgstr "no"

#~ msgid "non-US"
#~ msgstr "non-US"

#~ msgid "packages that contain files or directories named like this"
#~ msgstr ""
#~ "paquets que contenen fitxers o directoris anomenats d'aquesta manera"

#~ msgid "yes"
#~ msgstr "sí"
