# Emmanuel Galatoulas <galas@tee.gr>, 2019, 2020.
# EG <galatoulas@cti.gr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2021-09-02 02:05+0300\n"
"Last-Translator: EG <galatoulas@cti.gr>\n"
"Language-Team: Greek <debian-l10n-greek@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.0\n"

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Ιστότοπος του Debian"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Αναζήτηση στον ιστότοπο του Debian."

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Debian"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr "Αναζήτηση στον ιστότοπο του Debian"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Ναι"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Όχι"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Σχέδιο Debian"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Το Debian ένα λειτουργικό σύστημα και μια διανομή Ελεύθερου Λογισμικού. "
"Συντηρείται και ανανεώνεται μέσα από το έργο πολλών χρηστών που διαθέτουν "
"εθελοντικά τον χρόνο και την προσπάθειά τους."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "debian, GNU, linux, unix, ανοιχτού κώδικα, ελεύθερο, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "Πίσω στην <a href=\"m4_HOME/\">Κεντρική σελίδα του Σχεδίου Debian</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Κεντρική Σελίδα"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Παράλειψη της γρήγορης πλοήγησης (Quicknav)"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "Σχετικά"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "Σχετικά με το Debian"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Επικοινωνήστε μαζί μας"

#: ../../english/template/debian/common_translation.wml:37
msgid "Legal Info"
msgstr "Νομικές Πληροφορίες"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr "Ιδιωτικότητα Δεδομένων"

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Δωρεές"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Γεγονότα"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Νέα"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Διανομή"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Υποστήριξη"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr "Καθαρά \"Μείγματα\""

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Η γωνία των προγραμματιστ(ρι)ών"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Τεκμηρίωση"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Πληροφορίες σχετικά με την ασφάλεια"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Αναζήτηση"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "τίποτα"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Εκκίνηση"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "διεθνώς"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Χάρτης σελίδων"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Διάφορα"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Αποκτώντας το Debian"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "Το Ιστολόγιο του Debian"

#: ../../english/template/debian/common_translation.wml:94
msgid "Debian Micronews"
msgstr "Μικρά Νέα του Σχεδίου Debian"

#: ../../english/template/debian/common_translation.wml:97
msgid "Debian Planet"
msgstr "Πλανήτης Debian"

#: ../../english/template/debian/common_translation.wml:100
msgid "Last Updated"
msgstr "Τελευταία επικαιροποίηση"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Παρακαλούμε στείλτε τα σχόλια, την κριτική και τις υποδείξεις σας σχετικά με "
"αυτές τις σελίδες στη λίστα αλληλογραφίας <a href=\"mailto:debian-doc@lists."
"debian.org\"></a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "δεν είναι απαραίτητο"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "μη διαθέσιμο"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "μη εφαρμόσιμο"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "στην έκδοση 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "στην έκδοση 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "στην έκδοση 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "στην έκδοση 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "στην έκδοση 2.2"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:89
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""
"Για να αναφέρετε ένα πρόβλημα με τον ιστότοπο, παρακαλούμε στείλτε ένα "
"μήνυμα στη δημόσια αρχειοθετημένη λίστα αλληλογραφίας <a href=\"mailto:"
"debian-www@lists.debian.org\">debian-www@lists.debian.org</a> στα Αγγλικά. "
"Για άλλες πληροφορίες επικοινωνίας, δείτε τη <a href=\"m4_HOME/contact"
"\">Σελίδα επικοινωνίας</a> του Debian. Ο πηγαίος κώδικας του ιστότοπου είναι "
"<a href=\"https://salsa.debian.org/webmaster-team/webwml\">διαθέσιμος εδώ</"
"a>."

#: ../../english/template/debian/footer.wml:92
msgid "Last Modified"
msgstr "Τελευταία τροποποίηση"

#: ../../english/template/debian/footer.wml:95
msgid "Last Built"
msgstr "Πιο πρόσφατη Έκδοση"

#: ../../english/template/debian/footer.wml:98
msgid "Copyright"
msgstr "Πνευματικά δικαιώματα"

#: ../../english/template/debian/footer.wml:101
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> και άλλοι·"

#: ../../english/template/debian/footer.wml:104
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr ""
"Δείτε τους <a href=\"m4_HOME/license\" rel=\"copyright\">όρους άδειας "
"χρήσης</a>"

#: ../../english/template/debian/footer.wml:107
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Το όνομα Debian είναι ένα καταχωρημένο <a href=\"m4_HOME/trademark"
"\">εμπορικό σήμα</a> της Software in the Public Interest, Inc."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Η σελίδα αυτή είναι επίσης διαθέσιμη και στις ακόλουθες γλώσσες:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr "Πώς να <a href=m4_HOME/intro/cn>ορίσετε την προκαθορισμένη γλώσσα</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr "Προεπιλογές Περιηγητή"

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr "Αναίρεση του cookie υπέρβασης της γλώσσας"

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Το Debian Διεθνώς"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Συνεργάτες"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Εβδομαδιαία Νέα του Debian"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Εβδομαδιαία Νέα"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Νέα του Σχεδίου Debian"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Νέα του Σχεδίου"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Πληροφορίες έκδοσης"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Πακέτα του Debian"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Μεταφόρτωση"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Το Debian&nbsp;σε&nbsp;CD"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Βιβλία για το Debian"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "Debian Wiki"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Αρχείο λιστών αλληλογραφίας"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Λίστες αλληλογραφίας"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Κοινωνικό Συμβόλαιο"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Κώδικας Συμπεριφοράς"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Debian 5.0 - The universal operating system"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Χάρτης των ιστοσελίδων του Debian"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Βάση δεδομένων προγραμματιστ(ρι)ών"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "Συχνές Ερωτήσεις για το Debian"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Το εγχειρίδιο της πολιτικής του Debian"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Αναφορά προγραμματιστ(ρι)ών"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Οδηγίες για νέους συντηρητές"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Σφάλματα κρίσιμα για την κυκλοφορία της έκδοσης"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Αναφορές Lintian"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Αρχεία για τις λίστες αλληλογραφίας χρηστών"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Αρχεία για τις λίστες αλληλογραφίας των προγραμματιστών"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Αρχεία για τις λίστες αλληλογραφίας i18n/l10n"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Αρχεία για τις λίστες αλληλογραφίας των υλοποιήσεων αρχιτεκτονικών"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr ""
"Αρχεία για τις λίστες αλληλογραφίας του Συστήματος Παρακολούθησης Σφαλμάτων"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Αρχεία για διάφορες λίστες αλληλογραφίας"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Ελεύθερο Λογισμικό"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Ανάπτυξη"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Βοηθήστε το Debian"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Αναφορές σφαλμάτων"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Υλοποιήσεις/Αρχιτεκτονικές"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Εγχειρίδιο εγκατάστασης"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "Πωλητές CD"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "Εικόνες ISO για CD/USB"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Δικτυακή εγκατάσταση"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Προ-εγκατεστημένο"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Σχέδιο Debian-Edu"

#: ../../english/template/debian/links.tags.wml:137
msgid "Salsa &ndash; Debian Gitlab"
msgstr "Salsa &ndash; Debian Gitlab"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Διασφάλιση Ποιότητας"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Σύστημα Παρακολούθησης Πακέτων "

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Επισκόπηση πακέτων των προγραμματιστ(ρι)ών του Debian"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Κεντρική Σελίδα Debian "

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Τίποτα για αυτό το έτος."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "έχει προταθεί"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "υπό συζήτηση"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "η ψηφοφορία είναι ανοιχτή"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "τελείωσε"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "αποσύρθηκε"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Μελλοντικά γεγονότα"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Παλιότερα γεγονότα"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(νέα αναθεώρηση)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "Αναφορά"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr "Η σελίδα ανακατευθύνθηκε στην <newpage/>"

#: ../../english/template/debian/redirect.wml:12
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""
"Το όνομα αυτής της σελίδας άλλαξε σε  <url <newpage/>>, παpακαλούμε "
"ανανεώστε τους συνδέσμους σας."

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s για %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Σημείωση:</em> To <a href=\"$link\">αυθεντικό</a> είναι νεώτερο από την "
"παρούσα μετάφραση."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Προσοχή! Αυτή η μετάφραση είναι πολύ παλιά, παρακαλούμε δείτε το <a href="
"\"$link\">αυθεντικό κείμενο</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr ""
"<em>Σημείωση:</em> To αυθεντικό κείμενο αυτής της μετάφρασης δεν είναι πλέον "
"διαθέσιμο."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr "Λάθος έκδοση μετάφρασης!"

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Πίσω στη σελίδα  <a href=\"../\">Ποιος χρησιμοποιεί το Debian;</a>."

#~ msgid "%0 (dead link)"
#~ msgstr "%0 (νεκρός σύνδεσμος)"

#~ msgid "%s  &ndash; %s, Version %s: %s"
#~ msgstr "%s  &ndash; %s, Version %s: %s"

#~ msgid "%s  &ndash; %s: %s"
#~ msgstr "%s  &ndash; %s: %s"

#~ msgid "&middot;"
#~ msgstr "&middot;"

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr "υπάρχουν διαθέσιμα <a href=\"../../\">Παλαιότερα τεύχη</a>."

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>Συντονιστής της</th><th>εργασίας</th>"

#~ msgid "<void id=\"dc_artwork\" />Artwork"
#~ msgstr "<void id=\"dc_artwork\" />Καλιτεχνικά"

#~ msgid "<void id=\"dc_download\" />Download"
#~ msgstr "<void id=\"dc_download\" />Μεταφόρτωση"

#~ msgid "<void id=\"dc_mirroring\" />Mirroring"
#~ msgstr "<void id=\"dc_mirroring\" />Καθρεπτισμοί"

#~ msgid "<void id=\"dc_misc\" />Misc"
#~ msgstr "<void id=\"dc_misc\" />Διάφορα"

#~ msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
#~ msgstr "<void id=\"dc_rsyncmirrors\" />Καθρεπτισμοί Rsync"

#~ msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgstr "<void id=\"dc_torrent\" />Μεταφόρτωση με Torrent"

#~ msgid "<void id=\"faq-bottom\" />faq"
#~ msgstr "<void id=\"faq-bottom\" />συχνές ερωτήσεις"

#~ msgid "<void id=\"misc-bottom\" />misc"
#~ msgstr "d id=\"misc-bottom\" />διάφορα"

#, fuzzy
#~| msgid ""
#~| "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~| "dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Τα Εβδομαδιαία νέα του Debian συντάσσονται απο τους "
#~ "<a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Τα Εβδομαδιαία νέα του Debian συντάσσονται απο τους "
#~ "<a href=\"mailto:dwn@debian.org\">%s</a>."

#, fuzzy
#~ msgid "<void id=\"plural\" />It was translated by %s."
#~ msgstr "Το παρόν τεύχος των ΕΝ μεταφράστηκε απο τους %s."

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Project News was edited by <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Τα Εβδομαδιαία νέα του Debian συντάσσονται απο τους "
#~ "<a href=\"mailto:dwn@debian.org\">%s</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Τα Εβδομαδιαία νέα του Debian συντάσσονται απο τους "
#~ "<a href=\"mailto:dwn@debian.org\">%s</a>."

#, fuzzy
#~ msgid "<void id=\"pluralfemale\" />It was translated by %s."
#~ msgstr ""
#~ "<void id=\"pluralfemale\" />Το παρόν τεύχος των ΕΝ μεταφράστηκε απο τις "
#~ "%s."

#, fuzzy
#~| msgid ""
#~| "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~| "dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Τα Εβδομαδιαία νέα του Debian συντάσσονται απο "
#~ "τον <a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Τα Εβδομαδιαία νέα του Debian συντάσσονται απο "
#~ "τον <a href=\"mailto:dwn@debian.org\">%s</a>."

#, fuzzy
#~ msgid "<void id=\"singular\" />It was translated by %s."
#~ msgstr "Το παρόν τεύχος των ΕΝ  μεταφράστηκε απο τον %s."

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Project News was edited by "
#~ "<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Τα Εβδομαδιαία νέα του Debian συντάσσονται απο "
#~ "τον <a href=\"mailto:dwn@debian.org\">%s</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Τα Εβδομαδιαία νέα του Debian συντάσσονται απο "
#~ "τον <a href=\"mailto:dwn@debian.org\">%s</a>."

#, fuzzy
#~ msgid "<void id=\"singularfemale\" />It was translated by %s."
#~ msgstr ""
#~ "<void id=\"singularfemale\" />Το παρόν τεύχος των ΕΝ μεταφράστηκε απο την "
#~ "%s."

#~ msgid "Amend&nbsp;a&nbsp;Proposal"
#~ msgstr "Πρόταση τροπολογίας"

#~ msgid "Architecture-independent component:"
#~ msgstr "Στοιχείο ανεξάρτητο αρχιτεκτονικής:"

#~ msgid "Back to other <a href=\"./\">Debian news</a>."
#~ msgstr "Πίσω στην σελίδα των <a href=\"./\">Νέων του Debian</a>."

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "Πίσω στην σελίδα των <a href=\"./\">συμβούλων του Debian</a>."

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Πίσω στην σελίδα των <a href=\"./\">ομιλητών του Debian</a>."

#, fuzzy
#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr "Πίσω στην <a href=\"m4_HOME/\">Κεντρική σελίδα του Debian</a>."

#~ msgid "Buy CDs or DVDs"
#~ msgstr "Αγορά CD ή DVD"

#~ msgid "CERT's vulnerabilities, advisories and incident notes:"
#~ msgstr "Σημειώσεις του CERT για περιστατικά και τρωτά σημεία:"

#~ msgid "Date published"
#~ msgstr "Ημερομηνία Εκδοσης"

#, fuzzy
#~ msgid "Debate"
#~ msgstr "Ομάδα CD του Debian"

#~ msgid "Debian CD team"
#~ msgstr "Ομάδα CD του Debian"

#~ msgid "Debian Involvement"
#~ msgstr "Συμμετοχή Debian"

#~ msgid "Debian Security Advisories"
#~ msgstr "Συμβουλές ασφάλειας"

#~ msgid "Debian Security Advisory"
#~ msgstr "Συμβουλή ασφάλειας"

#~ msgid "Decided"
#~ msgstr "Έχει αποφασιστεί"

#~ msgid "Discussion"
#~ msgstr "Συζήτηση"

#~ msgid "Download calendar entry"
#~ msgstr "Λήψη ημερολογιακής εγγραφής"

#~ msgid "Download via HTTP/FTP"
#~ msgstr "λήψη μέσω HTTP/FTP"

#~ msgid "Download with Jigdo"
#~ msgstr "Μεταφόρτωση με Jigdo"

#~ msgid "Fixed in"
#~ msgstr "Επιδιορθώθηκε την"

#~ msgid "Follow&nbsp;a&nbsp;Proposal"
#~ msgstr "Ακολουθία πρότασης"

#~ msgid "Free"
#~ msgstr "Ελεύθερο"

#~ msgid "Home&nbsp;Vote&nbsp;Page"
#~ msgstr "Κεντρική σελίδα ψηφοφορίας"

#~ msgid "How&nbsp;To"
#~ msgstr "Πώς να ψηφίσετε"

#~ msgid "In Mitre's CVE dictionary:"
#~ msgstr "Στο λεξικό Mitre's CVE:"

#~ msgid "In the Bugtraq database (at SecurityFocus):"
#~ msgstr "Στη βάση δεδομένων (στο SecurityFocus):"

#~ msgid "In&nbsp;Discussion"
#~ msgstr "Σε εξέλιξη"

#~ msgid "Latest News"
#~ msgstr "Τελευταία Νέα"

#~ msgid "License"
#~ msgstr "Αδεια Χρήσης"

#~ msgid "License Information"
#~ msgstr "Πληροφορίες Αδειας Χρήσης"

#~ msgid "List of Consultants"
#~ msgstr "Κατάλογος συμβούλων"

#~ msgid "List of Speakers"
#~ msgstr "Κατάλογος Ομιλητών"

#, fuzzy
#~ msgid ""
#~ "MD5 checksums of the listed files are available in the <a href=\"<get-var "
#~ "url />\">original advisory</a>."
#~ msgstr ""
#~ "Το άθροισμα ελέγχου MD5 των εικονιζομένων αρχείων είναι διαθέσιμο στη <a "
#~ "href=\"%attributes\">αρχική συμβουλή</a>."

#~ msgid "Main Coordinator"
#~ msgstr "Κύριος συντονιστής"

#, fuzzy
#~ msgid "Minimum Discussion"
#~ msgstr "Συζήτηση"

#~ msgid "More Info"
#~ msgstr "Περισσότερες πληροφορίες"

#~ msgid "More information"
#~ msgstr "Περισσότερες πληροφορίες"

#~ msgid "More information:"
#~ msgstr "Περισσότερες πληροφορίες:"

#~ msgid "Network Install"
#~ msgstr "Δικτυακή Eγκατάσταση"

#, fuzzy
#~ msgid "No Requested packages"
#~ msgstr "Πακέτα που επηρεάζονται"

#~ msgid "No other external database security references currently available."
#~ msgstr ""
#~ "Δεν υπάρχουν διαθέσιμες άλλες εξωτερικές αναφορές στην βάση αυτή τη "
#~ "στιγμή."

#, fuzzy
#~ msgid "No requests for adoption"
#~ msgstr "Περισσότερες πληροφορίες"

#~ msgid "Nobody"
#~ msgstr "Κανένας"

#, fuzzy
#~ msgid "Nominations"
#~ msgstr "Δωρεές"

#~ msgid "Non-Free"
#~ msgstr "Μη-Ελεύθερο"

#~ msgid "Other"
#~ msgstr "Άλλα"

#~ msgid "Please use English when sending mail."
#~ msgstr "Παρακαλούμε χρησιμοποιήστε Αγγλικά για αποστολή μηνύματος."

#, fuzzy
#~ msgid "Proposer"
#~ msgstr "πρόταση"

#~ msgid "Rating:"
#~ msgstr "Κατάταξη"

#~ msgid "Read&nbsp;a&nbsp;Result"
#~ msgstr "Ανάγνωση αποτελεσμάτων"

#~ msgid "Related Links"
#~ msgstr "Σχετικοί σύνδεσμοι"

#~ msgid "Security database references"
#~ msgstr "Αναφορές βάσης δεδομένων ασφαλείας"

#~ msgid ""
#~ "See the Debian <a href=\"m4_HOME/contact\">contact page</a> for "
#~ "information on contacting us."
#~ msgstr ""
#~ "Δείτε την σελίδα <a href=\"m4_HOME/contact\"> επικοινωνίας με το Debian</"
#~ "a> για πληροφορίες επικοινωνίας μαζί μας."

#~ msgid "Select a server near you"
#~ msgstr "Επιλέξτε έναν κοντινό σας εξυπηρετητή"

#~ msgid "Source:"
#~ msgstr "Κώδικας:"

#~ msgid "Submit&nbsp;a&nbsp;Proposal"
#~ msgstr "Κάντε τις προτάσεις σας"

#~ msgid "Summary"
#~ msgstr "Σύνοψη"

#~ msgid "Taken by:"
#~ msgstr "Το ανέλαβε ο:"

#, fuzzy
#~| msgid ""
#~| "To receive this newsletter weekly in your mailbox, <a href=\"http://"
#~| "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~| "list</a>."
#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"http://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Για να λαμβάνετε αυτό το φυλλάδιο καθε εβδομάδα, κάντε <a href=\"http://"
#~ "lists.debian.org/debian-news/\">συνδρομή</a> στον κατάλογο συνδρομητών "
#~ "debian-news."

#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"http://lists."
#~ "debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
#~ msgstr ""
#~ "Για να λαμβάνετε αυτό το φυλλάδιο καθε εβδομάδα, κάντε <a href=\"http://"
#~ "lists.debian.org/debian-news/\">συνδρομή</a> στον κατάλογο συνδρομητών "
#~ "debian-news."

#~ msgid "Upcoming Attractions"
#~ msgstr "Επερχόμενα γεγονότα"

#~ msgid "Version"
#~ msgstr "Εκδοση"

#~ msgid "Visit the site sponsor"
#~ msgstr "Επισκεφθείτε το χορηγό των σελίδων"

#~ msgid "Vote"
#~ msgstr "Ψηφοφορία"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "Έχει αρχίσει ψηφοφορία"

#~ msgid "Vulnerable"
#~ msgstr "τρωτά"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "Σε αναμονή χορηγών"

#~ msgid "When"
#~ msgstr "Πότε"

#~ msgid "Where"
#~ msgstr "Που"

#~ msgid "Withdrawn"
#~ msgstr "αποσύρθηκε"

#~ msgid "buy"
#~ msgstr "αγορά"

#~ msgid "buy pre-made images"
#~ msgstr "αγορά έτοιμων ειδώλων"

#~ msgid "debian_on_cd"
#~ msgstr "debian_σε_cd"

#~ msgid "discussed"
#~ msgstr "έχουν συζητηθεί"

#~ msgid "download with pik"
#~ msgstr "λήψη με το pik"

#~ msgid "free"
#~ msgstr "ελευθερο"

#~ msgid "http_ftp"
#~ msgstr "http_ftp"

#~ msgid "jigdo"
#~ msgstr "jigdo"

#~ msgid "link may no longer be valid"
#~ msgstr "Ο σύνδεσμος μπορεί να μήν είναι έγκυρος"

#~ msgid "net_install"
#~ msgstr "δικτυακή_εγκατάσταση"

#~ msgid "network install"
#~ msgstr "δικτυακή εγκατάσταση"

#~ msgid "non-free"
#~ msgstr "μη-ελευθερο"
