#use wml::debian::template title="Debian 8 -- Problemen Rapporteren" BARETITLE=true
#use wml::debian::translation-check translation="b66f2524a8b8f14cc26741ffcb046ee1396dd23a"

# Translators: copy of squeeze/reportingbug

<h2><a name="report-release">Met de Notities bij de Release</a></h2>

<p>Fouten in de <a href="releasenotes">Notities bij de Release</a> moeten
<a href="$(HOME)/Bugs/Reporting">gerapporteerd worden als bug</a> tegen het
pseudo-pakket <tt>release-notes</tt>. Discussie met betrekking tot dat document
wordt gecoördineerd via de mailinglijst debian-doc op
<a href="mailto:debian-doc@lists.debian.org">\
&lt;debian-doc@lists.debian.org&gt;</a>. Als u problemen vindt in verband met
het document die niet geschikt zijn voor een bug, kunt u waarschijnlijk beter de
lijst mailen.
</p>

<h2><a name="report-installation">Met de installatie</a></h2>

<p>Als u een probleem heeft met het installatiesysteem, rapporteer dan bugs
tegen het pakket <tt>installation-reports</tt>. Vul het
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">rapportsjabloon</a>
in om ervoor te zorgen dat u alle benodigde informatie opneemt.</p>

<p>Indien u suggesties of correcties heeft voor de
<a href="installmanual">Installatiehandleiding</a>, dan zou u deze moeten
<a href="$(HOME)/Bugs/Reporting">rapporteren als bugs</a> tegen
<tt>installation-guide</tt>, dat het broncodepakket is waarin de documentatie
wordt onderhouden.</p>

<p>Als u problemen heeft met het installatiesysteem die niet geschikt zijn voor
een bug (bijv. u bent niet zeker of het echt een bug is of niet, het is
onduidelijk welk deel van het systeem voor het probleem zorgt, enz.) kunt u
waarschijnlijk het beste een email sturen naar de mailinglijst
<a href="mailto:debian-boot@lists.debian.org">\
&lt;debian-boot@lists.debian.org&gt;</a>.</p>

<h2><a name="report-upgrade">Met een upgrade</a></h2>

<p>Als u problemen ondervindt bij het opwaarderen van uw systeem vanaf een vorige
release, dan kunt u een bug indienen tegen het pakket <tt>upgrade-reports</tt>,
dat het pseudo-pakket is dat wordt gebruikt om die informatie bij te houden. Voor
meer informatie over het indienen van opwaarderingsrapporten kunt u de
<a href="releasenotes">Notities bij de release</a> lezen.</p>

<h2><a name="report-package">Andere problemen</a></h2>

<p>Als u na de installatie problemen heeft met het systeem, moet u proberen het
pakket op te sporen dat de problemen veroorzaakt en een
<a href="$(HOME)/Bugs/Reporting">bugrapport indienen</a> tegen dat pakket.</p>
