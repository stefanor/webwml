#use wml::debian::template title="Het mechanisme van <q>proposed-updates</q>"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="09c8de4214e938e26eeaf5c5d5bbb5937213f414"

<p>Alle wijzigingen aan de uitgebrachte <q>stabiele</q> (en <q>oude stabiele</q>)
distributie ondergaan een langere testperiode voordat ze in het archief worden
geaccepteerd. Elke dergelijke update van de stabiele (oude stabiele) release
wordt een <q>tussenrelease</q> genoemd.</p>

<p>De voorbereiding van tussenreleases gebeurt via het mechanisme van
<q>proposed-updates</q>. Bijgewerkte pakketten worden geüpload naar een aparte
wachtrij met als naam <code>p-u-new</code> (<code>o-p-u-new</code>), voordat ze
worden geaccepteerd in <q>proposed-updates</q>
(<q>oldstable-proposed-updates</q>).
</p>

<p>Om deze pakketten te gebruiken met APT, kunt u de volgende regels toevoegen
aan uw bestand <code>sources.list</code>:</p>

<pre>
  \# voorgestelde updates voor de volgende tussenrelease
  deb http://ftp.us.debian.org/debian <current_release_name>-proposed-updates main contrib non-free
</pre>

<p>Merk op dat dit beschikbaar is op
<a href="$(HOME)/mirror/list">de /debian/ spiegelservers</a>. Het is niet nodig
om deze specifieke spiegelserver te gebruiken. De bovenstaande keuze voor
ftp.us.debian.org is enkel bij wijze van voorbeeld.</p>

<p>Nieuwe pakketten arriveren in proposed-updates wanneer ontwikkelaars van
Debian ze ofwel uploaden naar <q>proposed-updates</q>
(<q>oldstable-proposed-updates</q>), of naar <q>stable</q> (<q>oldstable</q>).
Het <a href="$(HOME)/doc/manuals/developers-reference/pkgs.html#upload-stable">\
uploadproces</a> wordt beschreven in de referentiehandleiding voor ontwikkelaars.
</p>

<p>Merk op dat pakketten uit
<a href="$(HOME)/security/">security.debian.org</a> automatisch gekopieerd
worden naar de map p-u-new (o-p-u-new). Tegelijkertijd worden pakketten die
direct naar proposed-updates (oldstable-proposed-updates) worden geüpload, niet
door het Debian beveiligingsteam gecontroleerd.</p>

<p>De huidige lijst van pakketten die zich in de wachtrij p-u-new (o-p-u-new)
bevinden is te zien op
<url "https://release.debian.org/proposed-updates/stable.html">
(<url "https://release.debian.org/proposed-updates/oldstable.html">).</p>
